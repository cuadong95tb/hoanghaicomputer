import $ from 'jquery';
import 'slick-carousel';
import {handleHeaderEvents} from "../common/common";

$(function () {
    handleHeaderEvents();
    $('.advertise__slider').slick({
        infinite: true,
        dots: false,
        arrows: true,
        speed: 1000,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        prevArrow: '<button type="button" class="slick__prev"><i class="fal fa-chevron-left"></i></button>',
        nextArrow: '<button type="button" class="slick__next"><i class="fal fa-chevron-right"></i></button>',
    });
});
