import $ from 'jquery';
import "bootstrap/js/dist/modal";
import {handleHeaderEvents} from "../common/common";

function showModalThumbnail(event){
    event.preventDefault();
    let img = $(this).attr('href');
    if(img.length){
        $('.modal-show-thumbnail__img').attr('src',img);
        $('.modal-show-thumbnail').modal('show');
    }
}
$(function () {
    handleHeaderEvents();
    $(document).on('click', '.show-modal-thumbnail', showModalThumbnail);
});

