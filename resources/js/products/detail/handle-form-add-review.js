import $ from 'jquery';
import swal from 'sweetalert';
import * as formHanding from '../../common/form-handling';

function contentReviewValid(elm) {
    let minlength = $(elm).attr('minlength'),
        flag = formHanding.checkLengthTextarea(elm, `Mời bạn đánh giá sản phẩm (Tối thiểu ${minlength} kí tự)`);
    return flag;
}
function validateFormAddReview(event) {
    event.preventDefault();
    let checkContentValid = contentReviewValid($('#txt-review-content')),
        checkRating = formHanding.checkEmptyRadio('rating', 'Vui lòng chọn đánh giá của bạn về sản phẩm này!'),
        flag = checkContentValid && checkRating ? true : false,
        status = 0; //demo open modal
    if (flag){
        switch (status) {
            case 0 : {
                $('.modal-complete-send-review').modal('show');
                break;
            }
            case 1 : {
                $(this)[0].submit();
                break;
            }
            case 2 : {
                swal({
                    title: "Xin lỗi!",
                    text: "Sản phẩm này bạn chưa sử dụng hoặc đã đánh giá!",
                    icon: "warning",
                    dangerMode: true,
                });
                break;
            }
            default : {}
        }
    }
}

function nameValid($elm){
    let flag = formHanding.checkEmpty($elm, 'Vui lòng nhập họ tên của bạn!');
    return flag;
}
function telValid($elm) {
    let checkValueEmpty = formHanding.checkEmpty($elm, 'Vui lòng nhập số điện thoại của bạn!'),
        flag = checkValueEmpty ? formHanding.checkPhoneNumberFormat($elm) : false;
    return flag;
}
function emailValid($elm) {
    let flag = true;
    if ($elm.val()){
        flag = formHanding.checkEmailFormat($elm);
    }else {
        flag = true;
        $elm.removeClass('error');
        $('[data-error="' + $elm.attr('id') + '"]').text('');
    }
    return flag;
}
function submitFormCompleteSendReview(event){
    event.preventDefault();
    let checkNameValid = nameValid($('#name-user-send-review')),
        checkTelValid = telValid($('#phone-user-send-review')),
        checkEmailValid = emailValid($('#email-user-send-review')),
        flag = (checkNameValid && checkTelValid && checkEmailValid) ? true : false;
    if (flag){
        //ajax submit... then:...
        $('.modal-complete-send-review').modal('hide');
        $('.form-add-review').trigger('reset');
    }
}

export function handleFormAddReview() {
    $('.reviews-and-comments').on('submit', '.form-add-review', validateFormAddReview);
    $('.form-add-review').on('change keyup', '#txt-review-content', function () {
        contentReviewValid(this);
    });
    $('.form-add-review').on('change', 'input[name="rating"]', function () {
        formHanding.checkEmptyRadio('rating');
    });

    //handing modal complete send review
    $(document).on('change keyup invalid', '#name-user-send-review', function () {
        nameValid($(this));
    });
    $(document).on('change keyup invalid', '#phone-user-send-review', function () {
        telValid($(this));
    });
    $(document).on('change keyup invalid', '#email-user-send-review', function () {
        emailValid($(this));
    });
    $(document).on('submit', '.form-complete-send-review', submitFormCompleteSendReview);
}