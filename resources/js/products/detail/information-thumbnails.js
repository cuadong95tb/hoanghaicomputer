import $ from 'jquery';
import 'easyzoom';

export function handleInformationThumbnails(){
    let $easyzoom = $('.easyzoom').easyZoom(),
        apiZoom = $easyzoom.filter('.easyzoom--with-thumbnails').data('easyZoom');
    $('.information-thumbnails__list').on('click', 'a', function(event) {
        event.preventDefault();
        let $this = $(this);
        apiZoom.swap($this.data('standard'), $this.attr('href'));
        $('.information-thumbnails__list a.active').removeClass('active');
        $this.addClass('active')
    });
}
