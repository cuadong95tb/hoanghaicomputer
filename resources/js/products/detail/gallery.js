import $ from 'jquery';
window.jQuery = $;
require("@fancyapps/fancybox");
import 'slick-carousel';
export function handleGallery() {
    $('[data-fancybox="product-gallery"]').fancybox({
        protect: true,
        image: {
            preload: true
        },
        buttons: [
            "slideShow",
            "fullScreen",
            "thumbs",
            "close"
        ],
        thumbs: {
            autoStart: true,
            hideOnClose: false,
            parentEl: ".fancybox-container",
            axis: "y"
        },
    });
    $('.gallery__slider').slick({
        dots: false,
        infinite: true,
        speed: 1000,
        slidesToShow: 5,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        pauseOnHover: true,
        prevArrow: '<button type="button" class="slick__prev"><i class="fal fa-chevron-left"></i></button>',
        nextArrow: '<button type="button" class="slick__next"><i class="fal fa-chevron-right"></i></button>',
        responsive: [
            {
                breakpoint: 767,
                settings: {
                    slidesToShow: 3,
                }
            },
            {
                breakpoint: 500,
                settings: {
                    slidesToShow: 2,
                }
            },
        ]
    })
}