import $ from 'jquery';
function chooseColor() {
    $('.choose-color label.active').removeClass('active');
    $(this).addClass('active');
}
export function handleInformationActions(){
    $('.choose-color').on('click', 'label', chooseColor);
}