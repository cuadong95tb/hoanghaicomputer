import $ from 'jquery';
import {handleFormAddReview} from "./handle-form-add-review";

function openFormAddReview(event) {
    event.preventDefault();
    $('#form-add-review').slideDown();
}

function closeFormAddReview() {
    $('#form-add-review').find('.error').removeClass('error');
    $('#form-add-review').find('.alert-error').text('');
    $('#form-add-review').slideUp();
}

function likeReview(event) {
    event.preventDefault();
    let $this = $(this);
    let numberLike = parseFloat($this.text());
    $this.text($this.hasClass('active') ? (numberLike - 1) : (numberLike + 1));
    $this.toggleClass('active');
}

export function handleReviewsAndComments() {
    $('.reviews-and-comments').on('click', '.open-form-review', openFormAddReview);
    $('.reviews-and-comments').on('click', '.close-form-review', closeFormAddReview);
    handleFormAddReview();

    $('.reviews-and-comments').on('click', '.button-like', likeReview);
}