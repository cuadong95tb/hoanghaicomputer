import $ from 'jquery';
import * as formHanding from '../../common/form-handling';


function nameValid($elm){
    let flag = formHanding.checkEmpty($elm, 'Vui lòng nhập họ tên của bạn!');
    return flag;
}

function telValid($elm) {
    let flag = true;
    if ($elm.val()){
        flag = formHanding.checkPhoneNumberFormat($elm);
    }else {
        flag = true;
        $elm.removeClass('error');
        $('[data-error="' + $elm.attr('id') + '"]').text('');
    }
    return flag;
}
function emailValid($elm) {
    let flag = true;
    if ($elm.val()){
        flag = formHanding.checkEmailFormat($elm);
    }else {
        flag = true;
        $elm.removeClass('error');
        $('[data-error="' + $elm.attr('id') + '"]').text('');
    }
    return flag;
}

function submitFormCompleteSendQuestion(event){
    event.preventDefault();
    let checkNameValid = nameValid($('#name-user-send-question')),
        checkTelValid = telValid($('#phone-user-send-question')),
        checkEmailValid = emailValid($('#email-user-send-question')),
        flag = (checkNameValid && checkTelValid && checkEmailValid) ? true : false;
    if (flag){
        //ajax submit... then:...
        $('.modal-complete-send-question').modal('hide');
        $('.form-add-question').trigger('reset');
    }
}

function contentInteractiveValid(that) {
    let $this = $(that),
        minLength = $this.attr('minlength'),
        error = $this.next(),
        flag = true;
    if ($this.val().length < minLength){
        $this.addClass('error');
        error.text(`Mời bạn viết bình luận (Tối thiểu ${minLength} ký tự).`);
        flag = false;
    }else {
        $this.removeClass('error');
        error.text('');
        flag = true;
    }
    return flag;
}
function validateFormAddInteractive(event) {
    event.preventDefault();
    let $this = $(this),
        textContent = $this.find('.txt-interactive-content'),
        checkContentValid = contentInteractiveValid(textContent),
        status = 0; //demo open modal
    if (checkContentValid){
        switch (status) {
            case 0 : {
                $('.modal-complete-send-question').modal('show');
                break;
            }
            case 1 : {
                $(this)[0].submit();
                break;
            }
            default : {}
        }
    }
}

function openFormReply(event) {
    event.preventDefault();
    let boxAddReply = $(this).parents('.item-interactive').find('.item-interactive__add-reply').slideDown();
}
export function handleQuestionsAndAnswers() {
    $('.form-add-interactive').on('change keyup', '.txt-interactive-content', function () {
        contentInteractiveValid(this);
    });
    $('.questions-and-answers').on('submit', '.form-add-interactive', validateFormAddInteractive);

    //handing modal complete send review
    $(document).on('change keyup invalid', '#name-user-send-question', function () {
        nameValid($(this));
    });
    $(document).on('change keyup invalid', '#phone-user-send-question', function () {
        telValid($(this));
    });
    $(document).on('change keyup invalid', '#email-user-send-question', function () {
        emailValid($(this));
    });
    $(document).on('submit', '.form-complete-send-question', submitFormCompleteSendQuestion);

    //item-interactive actions
    $('.item-interactive__actions').on('click', '.action-reply', openFormReply)
}
