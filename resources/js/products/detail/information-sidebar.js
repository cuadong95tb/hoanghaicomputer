import $ from 'jquery';
window.jQuery = $;
require("@fancyapps/fancybox");
export function handleInformationSidebar(){
    $('[data-fancybox="product-box"]').fancybox({
        protect: true,
        image: {
            preload: true
        },
        buttons: [
            "zoom",
            "slideShow",
            "close"
        ],
    });
}