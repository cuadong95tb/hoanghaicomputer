import $ from 'jquery';
import 'slick-carousel';
export function handleSliderRelatedAccessories() {
    $('.related-accessories__slider').slick({
        dots: false,
        infinite: true,
        speed: 1000,
        slidesToShow: 5,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        pauseOnHover: true,
        prevArrow: '<button type="button" class="slick__prev"><i class="fal fa-chevron-left"></i></button>',
        nextArrow: '<button type="button" class="slick__next"><i class="fal fa-chevron-right"></i></button>',
        responsive: [
            {
                breakpoint: 991,
                settings: {
                    slidesToShow: 4,
                }
            },
            {
                breakpoint: 767,
                settings: {
                    slidesToShow: 3,
                }
            },
            {
                breakpoint: 575,
                settings: {
                    slidesToShow: 2,
                }
            },
            {
                breakpoint: 400,
                settings: {
                    slidesToShow: 1,
                }
            }
        ]
    })
}