import $ from 'jquery';
import 'bootstrap/js/dist/modal';
import {handleHeaderEvents} from "../common/common";
import {handleInformationThumbnails} from "./detail/information-thumbnails";
import {handleInformationActions} from "./detail/information-actions";
import {handleInformationSidebar} from "./detail/information-sidebar";
import {handleSliderRelatedAccessories} from './detail/related-accessories';
import {handleReviewsAndComments} from './detail/reviews-and-comments';
import {handleQuestionsAndAnswers} from './detail/questions-and-answers';
import {handleGallery} from './detail/gallery';


function scrollToBoxInteractive(event){
    event.preventDefault();
    let that = $(this),
        id = that.attr('href'),
        nav = that.parents('.box-interactive__nav'),
        topDistance = nav.height() + parseFloat(nav.css('top'));
    if($(id).length){
        $('html, body').animate({
            scrollTop: $(id).offset().top - topDistance
        }, 500);
    }
    return false;
}

function activeMenuScrollInteractive(){
    let scrollDistance = $(this).scrollTop(),
        nav = $('.box-interactive__nav'),
        topDistance = nav.height() + parseFloat(nav.css('top')) + 10,
        section = $('.box-interactive .active-menu-scroll');
    section.each(function (i) {
        let top = $(this).offset().top - topDistance;
        if (scrollDistance >= top) {
            nav.find('li.active').removeClass('active');
            nav.find('li').eq(i).addClass('active');
        }
    });
}

$(function () {
    handleHeaderEvents();
    handleInformationThumbnails();
    handleInformationActions();
    handleInformationSidebar();
    handleSliderRelatedAccessories();
    handleReviewsAndComments();
    handleQuestionsAndAnswers();
    handleGallery();

    //scroll box interactive
    $('.box-interactive__nav').on('click', 'a', scrollToBoxInteractive);
    $(window).on('scroll', activeMenuScrollInteractive);
});
