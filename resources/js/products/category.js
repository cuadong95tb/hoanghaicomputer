import $ from 'jquery';
import {handleHeaderEvents} from "../common/common";

function showDetailCategory(event){
    event.preventDefault();
    $('.detail-category').addClass('show');
    $(this).remove();
}
$(function () {
    handleHeaderEvents();
    $(document).on('click', '.detail-category__view-more', showDetailCategory)
});
