import $ from 'jquery';
import {handleHeaderEvents} from "../common/common";

$(function () {
    handleHeaderEvents();
});