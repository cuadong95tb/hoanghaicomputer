import $ from 'jquery';
import swal from 'sweetalert';
import {handleHeaderEvents} from "../common/common";
import {checkEmpty, checkPhoneNumberFormat, checkEmailFormat, customInputNumber, checkEmptyRadio} from "../common/form-handling";
import * as formHanding from "../common/form-handling";

function nameValid($elm){
    let flag = checkEmpty($elm, 'Vui lòng nhập họ tên của bạn!');
    return flag;
}
function telValid($elm) {
    let checkValueEmpty = checkEmpty($elm, 'Vui lòng nhập số điện thoại của bạn!'),
        flag = checkValueEmpty ? checkPhoneNumberFormat($elm) : false;
    return flag;
}
function emailValid($elm) {
    let flag = true;
    if ($elm.val()){
        flag = checkEmailFormat($elm);
    }else {
        flag = true;
        $elm.removeClass('error');
        $('[data-error="' + $elm.attr('id') + '"]').text('');
    }
    return flag;
}
function addressValid($elm){
    let flag = checkEmpty($elm, 'Vui lòng nhập địa chỉ giao hàng của bạn!');
    return flag;
}
function linkFacebookValid($elm){
    let flag = checkEmpty($elm, 'Vui lòng nhập đường link Facebook của bạn!');
    return flag;
}
function submitFormCart(event){
    event.preventDefault();
    let checkNameValid = nameValid($('#txt-name')),
        checkTelValid = telValid($('#txt-phone')),
        checkEmailValid = emailValid($('#txt-email')),
        checkAddressValid = addressValid($('#txt-address')),
        checkLinkFacebookValid = linkFacebookValid($('#txt-link-facebook')),
        checkPaymentValid = checkEmptyRadio('payment', 'Vui lòng chọn hình thức thanh toán!'),
        flag = (checkNameValid && checkTelValid && checkEmailValid && checkAddressValid && checkLinkFacebookValid && checkPaymentValid) ? true : false;
    if (flag){
        $(this)[0].submit();
    }
}

function toggleFormDiscount(event){
    event.preventDefault();
    $(this).toggleClass('checked');
    $('.form-discount').slideToggle();
}

function chooseColor(){
    $('.choose-color label.active').removeClass('active');
    $(this).addClass('active');
}

function removeProductFromCart(event){
    event.preventDefault();
    swal({
        title: "Bạn chắc chắn?",
        text: "Bạn chắc chắn muốn xóa sản phẩm này khỏi giỏ hàng?",
        icon: "warning",
        dangerMode: true,
    })
        .then(willDelete => {
            if (willDelete) {
                //handle remove product from cart...
            }
        });
}

$(function () {
    handleHeaderEvents();

    //toggle form discount
    $(document).on('click', '.use-discount', toggleFormDiscount);

    //choose color product
    $('.choose-color').on('click', 'label', chooseColor);

    //remove product from cart
    $(document).on('click', '.remove-from-cart', removeProductFromCart);

    //handle form cart
    customInputNumber();

    $('.form-cart').on('change', 'input[name="payment"]', function () {
        checkEmptyRadio('payment');
    });
    $(document).on('change keyup invalid', '#txt-name', function () {
        nameValid($(this));
    });
    $(document).on('change keyup invalid', '#txt-phone', function () {
        telValid($(this));
    });
    $(document).on('change keyup invalid', '#txt-email', function () {
        emailValid($(this));
    });
    $(document).on('change keyup invalid', '#txt-address', function () {
        addressValid($(this));
    });
    $(document).on('change keyup invalid', '#txt-link-facebook', function () {
        linkFacebookValid($(this));
    });
    $(document).on('submit', '.form-cart', submitFormCart);
});
