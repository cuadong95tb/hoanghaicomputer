import $ from 'jquery';

//handler Header Nav
function toggleNavChildren(e) {
    if (screen.width < 992){
        e.preventDefault();
        $(this).next().slideToggle();
    }
}

//Open and Close Box on Header
function openBoxOnHeader(event) {
    event.preventDefault();
    let box = $(this).attr('href');
    if(box){
        $(box).addClass('opened');
    }
}
function closeBoxOnHeader(event) {
    event.preventDefault();
    let box = $(this).attr('href');
    if(box){
        $(box).removeClass('opened');
    }
}

//fixed Header When Scroll Page
function fixedHeaderWhenScrollPage() {
    $(window).scroll(function () {
        var scrollTop = $(this).scrollTop(),
            heightHeader = $('.header').innerHeight();
        if (scrollTop > heightHeader + 15) {
            $('.header').addClass("header--fixed");
        } else {
            $('.header').removeClass("header--fixed");
        }
    });
}


//export
export function handleHeaderEvents(){
    fixedHeaderWhenScrollPage();
    $(document).on('click', '.nav-categories .has-child > a', toggleNavChildren);
    $(document).on('click', '.header .open-box', openBoxOnHeader);
    $(document).on('click', '.header .close-box', closeBoxOnHeader);

}
