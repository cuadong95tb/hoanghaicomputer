import $ from 'jquery';
export function checkEmpty(elm, errorText) {
    let that = $(elm),
        id = that.attr('id'),
        error = id ? $('[data-error="' + id + '"]') : '',
        flag = true;
    if (that.val()){
        flag = true;
        that.removeClass('error');
        error ? error.text('') : '';
    }else {
        that.addClass('error');
        error ? error.text(errorText) : '';
        flag = false;
    }
    return flag;
}
export function checkPhoneNumberFormat(elm) {
    let that = $(elm),
        value = that.val(),
        id = that.attr('id'),
        error = id ? $('[data-error="' + id + '"]') : '',
        tel_regex = /^[\+]?[(]?[\+]?[0-9]{2,4}[)]?[-\s\.]?[0-9]{2,4}[-\s\.]?[0-9]{1,6}$/im ,
        flag = true;
    if (tel_regex.test(value)){
        that.removeClass('error');
        error.text('');
        flag = true;
    }else {
        that.addClass('error');
        error.text('Vui lòng nhập đúng định dạng số điện thoại!');
        flag = false;
    }
    return flag;
}
export function checkEmailFormat(elm) {
    let that = $(elm),
        value = that.val(),
        id = that.attr('id'),
        error = id ? $('[data-error="' + id + '"]') : '',
        email_regex = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/ ,
        flag = true;
    if (email_regex.test(value)){
        that.removeClass('error');
        error.text('');
        flag = true;
    }else {
        that.addClass('error');
        error.text('Vui lòng nhập đúng định dạng email!');
        flag = false;
    }
    return flag;
}

export function checkLengthTextarea(elm, errorText){
    let that = $(elm),
        value = that.val(),
        id = that.attr('id'),
        error = id ? $('[data-error="' + id + '"]') : '',
        minLength = parseFloat(that.attr('minlength')),
        flag = true;
    if (value.length < minLength){
        that.addClass('error');
        error.text(errorText);
        flag = false;
    }else {
        that.removeClass('error');
        error.text('');
        flag = true;
    }
    return flag;
}

export function checkEmptyRadio(nameRadio, errorText){
    let name = nameRadio,
        error = $('[data-error="radio-' + name + '"]'),
        flag = true;
    if ($('input[type="radio"][name="'+name+'"]:checked').length){
        error.text('');
        flag = true;
    }else {
        error.text(errorText);
        flag = false;
    }
    return flag;
}

//custome input type number
function quantityUp() {
    let btn = $(this),
        input = $(this).parent().find('input[type="number"]'),
        min = input.attr('min'),
        max = input.attr('max'),
        oldValue = parseFloat(input.val()),
        newValue;
    newValue = oldValue >= max ? oldValue : (oldValue + 1);
    input.val(newValue);
    input.trigger("change");
}
function quantityDown() {
    let btn = $(this),
        input = $(this).parent().find('input[type="number"]'),
        min = input.attr('min'),
        max = input.attr('max'),
        oldValue = parseFloat(input.val()),
        newValue;
    newValue = oldValue <= min ? oldValue : (oldValue - 1);
    input.val(newValue);
    input.trigger("change");
}
export function customInputNumber() {
    $(document).on('click', '.quantity__up', quantityUp);
    $(document).on('click', '.quantity__down', quantityDown);
}