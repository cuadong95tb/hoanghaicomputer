@extends('frontend.layouts.blog-layout')
@section('meta')
    <title>Chi tiết bài viết</title>
@stop
@section('stylesheet')
    <link rel="stylesheet" href="{{asset('assets/blog/detail.css')}}">
@stop
@section('content')
    <main class="blog-detail-page">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 mb-5 mb-lg-0">
                    <article class="blog-detail">
                        <h1 class="blog-detail__title">Những điều Apple không nói về phiên bản MacBook Pro 13 inch 2020</h1>
                        <img class="blog-detail__thumbnail" src="https://storage.googleapis.com/shopdunk-images/tintucshopdunknew/2020/05/46ec0cbc-phien-ban-macbook-pro-13-inch-2020.jpg" alt="">
                        <div class="blog-detail__content text-format">
                            {{--content--}}
                            <p style="text-align: justify;" data-field="sapo">Ngay cả khi Covid-19 đang tàn phá nền kinh tế toàn cầu thì Apple vẫn cho ra mắt hàng loạt sản phẩm như iPhone SE, iPad Pro, MacBook Air mới và mới đây nhất chính là <strong>phiên bản MacBook Pro 13 inch 2020</strong>. Thiết bị được trang bị bàn phím mới, tăng gấp đôi dung lượng đặc biệt hiệu năng đồ họa nhanh hơn.</p>
                            <p style="text-align: justify;" data-field="sapo">Tuy nhiên bạn đã hiểu rõ về sản phẩm chưa? Có những điều mà nhà Táo không hề tiết lộ qua thông cáo báo chí. Vì vậy, hãy cùng <a href="https://shopdunk.com/">ShopDunk</a> khám phá nhé!</p>
                            <div id="toc_container" class="no_bullets"><p class="toc_title">Xem nhanh <span class="toc_toggle">[<a href="#">Ẩn</a>]</span></p><ul class="toc_list"><li><a href="#1_Ban_phim_cat_keo_moi_tren_phien_ban_MacBook_Pro_13_inch_2020">1. Bàn phím cắt kéo mới trên phiên bản MacBook Pro 13 inch 2020</a></li><li><a href="#2_Vien_man_hinh_day">2. Viền màn hình dày</a></li><li><a href="#3_Phien_ban_MacBook_Pro_13_inch_2020_khong_co_Wi-Fi_6">3. Phiên bản MacBook Pro 13 inch 2020 không có Wi-Fi 6</a></li><li><a href="#4_Hieu_nang_cau_hinh">4. Hiệu năng – cấu hình</a></li><li><a href="#5_Nhan_doi_dung_luong">5. Nhân đôi dung lượng</a></li></ul></div>
                            <h4 style="text-align: justify;"><span id="1_Ban_phim_cat_keo_moi_tren_phien_ban_MacBook_Pro_13_inch_2020">1. Bàn phím cắt kéo mới trên phiên bản MacBook Pro 13 inch 2020</span></h4>
                            <p style="text-align: justify;">Kể từ khi Apple ra mắt <a href="https://shopdunk.com/mac/">MacBook</a> Pro hoàn toàn mới vào năm 2016, nó đã thay đổi cơ chế bàn phím từ cắt kéo (scissor mechanism) sang bươm bướm (butterfly mechanism).</p>
                            <p style="text-align: justify;">Từ đó cũng bắt đầu vô số các lỗi ngớ ngẩn về bàn phím mà ngỡ là chẳng bao giờ có thể xảy ra trên một chiếc laptop như là kẹt phím, hành trình quá nông (gần như không có hành trình), độ ồn lớn….</p>
                            <div id="attachment_124899" style="width: 610px" class="wp-caption aligncenter"><img aria-describedby="caption-attachment-124899" class="size-large wp-image-124899" src="https://storage.googleapis.com/shopdunk-images/tintucshopdunknew/2020/05/945eaac6-phien-ban-macbook-pro-13-inch-2020-1-600x408.jpg" alt="phien-ban-macbook-pro-13-inch-2020-1" width="600" height="408" srcset="https://storage.googleapis.com/shopdunk-images/tintucshopdunknew/2020/05/945eaac6-phien-ban-macbook-pro-13-inch-2020-1.jpg 600w, https://storage.googleapis.com/shopdunk-images/tintucshopdunknew/2020/05/945eaac6-phien-ban-macbook-pro-13-inch-2020-1-300x204.jpg 300w, https://storage.googleapis.com/shopdunk-images/tintucshopdunknew/2020/05/945eaac6-phien-ban-macbook-pro-13-inch-2020-1-50x34.jpg 50w" sizes="(max-width: 600px) 100vw, 600px"><p id="caption-attachment-124899" class="wp-caption-text">Bàn phím cắt kéo mới trên phiên bản MacBook Pro 13 inch 2020</p></div>
                            <p style="text-align: justify;">Mặc dù Apple đã cải tiến qua các năm nhưng không thể phủ nhận rằng đó là thất bại và từ cuối năm ngoái ơn giời là họ đã thay đổi. Và kìa, đó là tất cả những gì đáng giá nhất bạn sẽ nhận được khi mua MacBook Pro 13 inch 2020.</p>
                            <h4 style="text-align: justify;"><span id="2_Vien_man_hinh_day">2. Viền màn hình dày</span></h4>
                            <p style="text-align: justify;">Nếu bạn kì vọng rằng MacBook Pro 13 inch sẽ được cải tiến màn hình mạnh mẽ bằng việc làm cho viền màn hình mỏng đi và nâng kích thước lên 14 inch.</p>
                            <div id="attachment_124900" style="width: 610px" class="wp-caption aligncenter"><img aria-describedby="caption-attachment-124900" class="size-large wp-image-124900" src="https://storage.googleapis.com/shopdunk-images/tintucshopdunknew/2020/05/21e1baea-phien-ban-macbook-pro-13-inch-2020-2-600x338.jpg" alt="phien-ban-macbook-pro-13-inch-2020-2" width="600" height="338" srcset="https://storage.googleapis.com/shopdunk-images/tintucshopdunknew/2020/05/21e1baea-phien-ban-macbook-pro-13-inch-2020-2-600x338.jpg 600w, https://storage.googleapis.com/shopdunk-images/tintucshopdunknew/2020/05/21e1baea-phien-ban-macbook-pro-13-inch-2020-2-300x169.jpg 300w, https://storage.googleapis.com/shopdunk-images/tintucshopdunknew/2020/05/21e1baea-phien-ban-macbook-pro-13-inch-2020-2-768x432.jpg 768w, https://storage.googleapis.com/shopdunk-images/tintucshopdunknew/2020/05/21e1baea-phien-ban-macbook-pro-13-inch-2020-2-50x28.jpg 50w, https://storage.googleapis.com/shopdunk-images/tintucshopdunknew/2020/05/21e1baea-phien-ban-macbook-pro-13-inch-2020-2.jpg 960w" sizes="(max-width: 600px) 100vw, 600px"><p id="caption-attachment-124900" class="wp-caption-text">Viền màn hình dày</p></div>
                            <p style="text-align: justify;">Tuy nhiên, <strong>phiên bản MacBook Pro 13 inch 2020</strong> sử dụng màn hình Retina 13 inch độ phân giải cao, độ sáng 500 nits và hỗ trợ cho gam màu rộng P3, công nghệ True Tone tùy chỉnh hiển thị phù hợp với điều kiện môi trường xung quanh.</p>
                            <p><strong>Xem thêm:</strong> <a title="Apple đã hết keo kiệt về RAM và SSD cho MacBook Pro 13 inch mới" href="https://tintuc.shopdunk.com/macbook-pro-13-inch-moi.html">Apple đã hết keo kiệt về RAM và SSD cho MacBook Pro 13 inch mới</a></p>
                            <h4 style="text-align: justify;"><span id="3_Phien_ban_MacBook_Pro_13_inch_2020_khong_co_Wi-Fi_6">3. Phiên bản MacBook Pro 13 inch 2020 không có Wi-Fi 6</span></h4>
                            <p style="text-align: justify;">MacBook Pro 2020 không tương thích với Wi-Fi 6 và vẫn sử dụng kết nối Wi-Fi 802.11ac cũ. Mặc dù trong cuộc sống ngày nay Wi-Fi 6 vẫn chưa phổ biến nhưng chỉ trong 1 hoặc 2 năm nữa là chúng sẽ nở rộ và là một cuộc cách mạng lớn về kết nối mạng không dây.</p>
                            <div id="attachment_124901" style="width: 610px" class="wp-caption aligncenter"><img aria-describedby="caption-attachment-124901" class="size-large wp-image-124901" src="https://storage.googleapis.com/shopdunk-images/tintucshopdunknew/2020/05/c0b46493-phien-ban-macbook-pro-13-inch-2020-3-600x387.jpg" alt="phien-ban-macbook-pro-13-inch-2020-3" width="600" height="387" srcset="https://storage.googleapis.com/shopdunk-images/tintucshopdunknew/2020/05/c0b46493-phien-ban-macbook-pro-13-inch-2020-3-600x387.jpg 600w, https://storage.googleapis.com/shopdunk-images/tintucshopdunknew/2020/05/c0b46493-phien-ban-macbook-pro-13-inch-2020-3-300x193.jpg 300w, https://storage.googleapis.com/shopdunk-images/tintucshopdunknew/2020/05/c0b46493-phien-ban-macbook-pro-13-inch-2020-3-768x495.jpg 768w, https://storage.googleapis.com/shopdunk-images/tintucshopdunknew/2020/05/c0b46493-phien-ban-macbook-pro-13-inch-2020-3-50x32.jpg 50w, https://storage.googleapis.com/shopdunk-images/tintucshopdunknew/2020/05/c0b46493-phien-ban-macbook-pro-13-inch-2020-3.jpg 960w" sizes="(max-width: 600px) 100vw, 600px"><p id="caption-attachment-124901" class="wp-caption-text">MacBook Pro 2020 không tương thích với Wi-Fi 6</p></div>
                            <p style="text-align: justify;">Hiện tại, iPhone 11 và&nbsp;iPad Pro&nbsp;2020 cũng đã sử dụng chuẩn kết nối Wi-Fi 6 mới này. Bên cạnh đó, chiếc máy tính xách tay mới nhất của Apple vẫn sử dụng camera FaceTime 720p dù ra mắt vào năm 2020.</p>
                            <p><strong>Xem thêm:</strong> <a title="5 lý do giúp hãng công nghệ Apple bình tĩnh bước qua đại dịch Covid-19" href="https://tintuc.shopdunk.com/hang-cong-nghe-apple.html">5 lý do giúp hãng công nghệ Apple bình tĩnh bước qua đại dịch Covid-19</a></p>
                            <h4 dir="ltr"><span id="4_Hieu_nang_cau_hinh">4. Hiệu năng – cấu hình</span></h4>
                            <p dir="ltr" style="text-align: justify;">Apple “thẳng thắn” chia sẻ rằng&nbsp;MacBook Pro 13.3 2020 sử dụng CPU Core i7 thế hệ thứ 10th 4 nhân với khả năng Turbo Boost lên tới 4.5GHz, trang bị 32GB RAM,… Tuy nhiên, đó chỉ là phiên bản&nbsp;MacBook Pro 13.3 có 04 cổng ThunderBolt mà thôi, còn phiên bản 02 cổng ThunderBolt lại là một câu chuyện khác hoàn toàn.</p>
                            <div id="attachment_124902" style="width: 610px" class="wp-caption aligncenter"><img aria-describedby="caption-attachment-124902" class="size-large wp-image-124902" src="https://storage.googleapis.com/shopdunk-images/tintucshopdunknew/2020/05/850e44e7-phien-ban-macbook-pro-13-inch-2020-4-600x400.jpg" alt="phien-ban-macbook-pro-13-inch-2020-4" width="600" height="400" srcset="https://storage.googleapis.com/shopdunk-images/tintucshopdunknew/2020/05/850e44e7-phien-ban-macbook-pro-13-inch-2020-4-600x400.jpg 600w, https://storage.googleapis.com/shopdunk-images/tintucshopdunknew/2020/05/850e44e7-phien-ban-macbook-pro-13-inch-2020-4-300x200.jpg 300w, https://storage.googleapis.com/shopdunk-images/tintucshopdunknew/2020/05/850e44e7-phien-ban-macbook-pro-13-inch-2020-4-50x33.jpg 50w, https://storage.googleapis.com/shopdunk-images/tintucshopdunknew/2020/05/850e44e7-phien-ban-macbook-pro-13-inch-2020-4.jpg 650w" sizes="(max-width: 600px) 100vw, 600px"><p id="caption-attachment-124902" class="wp-caption-text">Hiệu năng – cấu hình</p></div>
                            <p dir="ltr" style="text-align: justify;">Phiên bản MacBook Pro 13.3 2020 có 02 cổng ThunderBolt chỉ được trang bị CPU Intel thế hệ thứ 8th mà thôi… Nếu bạn không tin hãy truy cập trang chủ Apple mà xem, phiên bản “Max Option” 02 cổng ThunderBolt chỉ sử dụng Core i7 thế hệ thứ 8th với Turbo Boost lên đến 4.5GHz và 16GB RAM LPDDR3, 2TB SSD….</p>
                            <p dir="ltr" style="text-align: justify;">Chính vì vậy, đây là một thiệt thòi rất lớn đối với những ai chọn mua phiên bản 02 cổng ThunderBolt.</p>
                            <p dir="ltr"><strong>Xem thêm: </strong><a title="8 tựa game phiêu lưu và giải đố trên iPhone/ iPad hay nhất hiện nay" href="https://tintuc.shopdunk.com/tua-game-phieu-luu-va-giai-do.html">8 tựa game phiêu lưu và giải đố trên iPhone/ iPad hay nhất hiện nay</a></p>
                            <h4 style="text-align: justify;"><span id="5_Nhan_doi_dung_luong">5. Nhân đôi dung lượng</span></h4>
                            <p>MacBook Pro 13 inch 2020 hiện có dung lượng lưu trữ gấp đôi thế hệ trước, với dung lượng lưu trữ tiêu chuẩn bắt đầu từ 256GB cho đến 1TB. Do đó, khách hàng có thể lưu trữ nhiều ảnh, video và tệp hơn.</p>
                            <div id="attachment_124903" style="width: 610px" class="wp-caption aligncenter"><img aria-describedby="caption-attachment-124903" class="size-large wp-image-124903" src="https://storage.googleapis.com/shopdunk-images/tintucshopdunknew/2020/05/b77de699-phien-ban-macbook-pro-13-inch-2020-5-600x338.jpg" alt="phien-ban-macbook-pro-13-inch-2020-5" width="600" height="338" srcset="https://storage.googleapis.com/shopdunk-images/tintucshopdunknew/2020/05/b77de699-phien-ban-macbook-pro-13-inch-2020-5-600x338.jpg 600w, https://storage.googleapis.com/shopdunk-images/tintucshopdunknew/2020/05/b77de699-phien-ban-macbook-pro-13-inch-2020-5-300x169.jpg 300w, https://storage.googleapis.com/shopdunk-images/tintucshopdunknew/2020/05/b77de699-phien-ban-macbook-pro-13-inch-2020-5-768x432.jpg 768w, https://storage.googleapis.com/shopdunk-images/tintucshopdunknew/2020/05/b77de699-phien-ban-macbook-pro-13-inch-2020-5-50x28.jpg 50w, https://storage.googleapis.com/shopdunk-images/tintucshopdunknew/2020/05/b77de699-phien-ban-macbook-pro-13-inch-2020-5.jpg 960w" sizes="(max-width: 600px) 100vw, 600px"><p id="caption-attachment-124903" class="wp-caption-text">MacBook Pro 13 inch 2020 hiện có dung lượng lưu trữ gấp đôi thế hệ trước</p></div>
                            <p>Và đối với người dùng chuyên nghiệp, những người cần dung lượng lưu trữ lớn hơn cho các thư viện ảnh và dự án video lớn, MacBook Pro 13 inch mới hiện cung cấp ổ SSD 4TB.</p>
                            <p style="text-align: justify;"><strong>Xem thêm: </strong><a title="Tổng hợp 13 bài học từ Steve Jobs dành cho những nhà sáng lập startup" href="https://tintuc.shopdunk.com/bai-hoc-tu-steve-jobs.html">Tổng hợp 13 bài học từ Steve Jobs dành cho những nhà sáng lập startup</a></p>
                            <p style="text-align: justify;">Có thể thấy rằng, <strong>phiên bản MacBook Pro 13 inch 2020</strong> tuy không sở hữu viền màn hình mỏng, màn hình Mini-LED hay Wi-Fi 6 nhưng nâng cấp mà thiết bị mang lại chính là sự cải tiến về bàn phím cùng với dung lượng tăng gấp đôi để đáp ứng nhu cầu sử dụng cũng như nâng cao trải nghiệm người dùng lên một nấc mới.</p>
                            {{--end content--}}
                        </div>
                    </article>
                    <section class="advertise">
                        <div class="advertise__slider">
                            @for($i=0; $i<3; $i++)
                                <div class="advertise__item">
                                    <a href="#"><img src="https://storage.googleapis.com/shopdunk-images/tintucshopdunknew/2019/07/23d5e69b-co%CC%81-ne%CC%82n-mua-apple-watch-3-750x150.jpg" alt=""></a>
                                </div>
                            @endfor
                        </div>
                    </section>
                    <section class="blog-related">
                        <h3 class="blog-related__title">Có thể bạn quan tâm</h3>
                        <div class="row">
                            @for($i=0; $i<3; $i++)
                                <div class="col-6 col-sm-4">
                                    @include('frontend.blog.partials.blog-item-shortcut')
                                </div>
                            @endfor
                        </div>
                    </section>
                </div>
                <div class="col-lg-4">
                    @include('frontend.blog.partials.sidebar')
                </div>
            </div>
        </div>
    </main>
@stop
@section('script')
    <script src="{{asset('assets/blog/detail.js')}}"></script>
@stop