@extends('frontend.layouts.blog-layout')
@section('meta')
    <title>Danh mục bài viết</title>
@stop
@section('stylesheet')
    <link rel="stylesheet" href="{{asset('assets/blog/category.css')}}">
@stop
@section('content')
    <main class="category-blog-page">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 mb-5 mb-lg-0">
                    <h1 class="category-blog-page__title">
                        Iphone - IOS
                    </h1>
                    <section class="category-blog-page__desc">
                        <div class="text-format">
                            <p>Tổng hợp tất cả ứng dụng dành cho iPhone – iOS được cập nhập hằng ngày trên hoanghaicomputer.com</p>
                        </div>
                    </section>
                    <section class="category-blog-page__list">
                        @for ($i=0; $i<15; $i++)
                            @include('frontend.blog.partials.blog-item-full')
                        @endfor
                    </section>
                    <a href="" class="category-blog-page__view-more">Xem thêm <i class="fal fa-angle-double-down"></i></a>
                    @include('frontend.blog.partials.modal-show-thumbnail')
                </div>
                <div class="col-lg-4">
                    @include('frontend.blog.partials.sidebar')
                </div>
            </div>
        </div>
    </main>
@stop
@section('script')
    <script src="{{asset('assets/blog/category.js')}}"></script>
@stop
