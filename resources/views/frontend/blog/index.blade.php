@extends('frontend.layouts.blog-layout')
@section('meta')
    <title>Tin tức</title>
@stop
@section('stylesheet')
    <link rel="stylesheet" href="{{asset('assets/blog/index.css')}}">
@stop
@section('content')
    <main class="blog-page">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 mb-5 mb-lg-0">
                    @include('frontend.blog.partials.list-blog-icon')
                    <section class="blog-page__top">
                        <div class="row">
                            <div class="col-md-8">
                                @include('frontend.blog.partials.blog-item')
                            </div>
                            <div class="col-md-4">
                                <div class="row">
                                    <div class="col-sm-6 col-md-12">
                                        @include('frontend.blog.partials.blog-item-shortcut')
                                    </div>
                                    <div class="col-sm-6 col-md-12">
                                        @for($i=0; $i<3; $i++)
                                            <h3 class="blog-item--only-title">
                                                <a href="#">5 thủ thuật với camera iPhone SE 2020 để có thước phim chuyên nghiệp</a>
                                            </h3>
                                        @endfor
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section class="blog-page__list">
                        @for ($i=0; $i<5; $i++)
                            @include('frontend.blog.partials.blog-item')
                        @endfor
                        <a href="" title="">
                            <img class="d-block w-100" src="https://storage.googleapis.com/shopdunk-images/tintucshopdunknew/2019/07/07cb98d7-793x100.jpg" alt="">
                        </a>
                        @for ($i=0; $i<5; $i++)
                            @include('frontend.blog.partials.blog-item')
                        @endfor
                    </section>
                    <a href="" class="blog-page__view-more">Xem thêm <i class="fal fa-angle-double-down"></i></a>
                </div>
                <div class="col-lg-4">
                    @include('frontend.blog.partials.sidebar')
                </div>
            </div>
        </div>
    </main>
@stop
@section('script')
    <script src="{{asset('assets/blog/index.js')}}"></script>
@stop
