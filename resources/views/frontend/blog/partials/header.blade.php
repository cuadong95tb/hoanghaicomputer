<header class="header">
    <div class="container d-flex justify-content-between align-items-center">
        <div class="header__logo">
            <a href="/frontend/blog"><img src="{{asset('images/logo.png')}}" alt=""></a>
        </div>
        <div class="d-flex align-items-center">
            <div class="header__nav">
                <a href="#menu-mobile" class="open-box"><i class="fal fa-bars"></i></a>
                <div id="menu-mobile" class="header__nav-box">
                    <a href="#menu-mobile" class="close-box"><i class="fal fa-times"></i></a>
                    @include('frontend.blog.partials.nav-categories-blog')
                </div>
            </div>
            <div class="header__search ml-4">
                <a href="#form-search-blog" class="open-box"><i class="fal fa-search"></i></a>
                <div id="form-search-blog" class="header__search-box">
                    <div class="container">
                        @include('frontend.blog.partials.form-search-blog')
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>