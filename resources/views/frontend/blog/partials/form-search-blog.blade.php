<form action="" class="search-blog">
    <input type="text" class="search-blog__text" placeholder="Nhập từ tìm kiếm">
    <button type="submit" class="search-blog__submit"><i class="fal fa-search"></i></button>
    <a href="#form-search-blog" class="close-box"><i class="fal fa-times"></i></a>
</form>