<aside class="blog-sidebar">
    <section class="blog-sidebar__widget">
        <a href="" title="">
            <img class="d-block w-100" src="https://storage.googleapis.com/shopdunk-images/tintucshopdunknew/2019/09/2367a8f7-doi-iphone-cu-lay-iphone-moi-1.jpg" alt="">
        </a>
    </section>
    <section class="blog-sidebar__widget">
        <h2 class="blog-sidebar__title">Tin nổi bật</h2>
        <div class="blog-sidebar__hot-blog">
            <div class="row">
                @for ($i=0; $i<4; $i++)
                    <div class="col-6 col-md-3 col-lg-6">
                        @include('frontend.blog.partials.blog-item-shortcut')
                    </div>
                @endfor
            </div>
        </div>
    </section>
    <section class="blog-sidebar__widget">
        <a href="" title="">
            <img class="d-block w-100" src="https://storage.googleapis.com/shopdunk-images/tintucshopdunknew/2019/01/dtx.jpg" alt="">
        </a>
    </section>
    <section class="blog-sidebar__widget">
        <h2 class="blog-sidebar__title">Sản phẩm mới</h2>
        <div class="blog-sidebar__products">
            @for($i=0; $i<5; $i++)
                @include('frontend.partials.item-product-shortcut')
            @endfor
        </div>
    </section>
    <section class="blog-sidebar__widget">
        <a href="" title="">
            <img class="d-block w-100" src="https://storage.googleapis.com/shopdunk-images/tintucshopdunknew/2019/02/22f61172-banner-ios.png" alt="">
        </a>
    </section>
    <section class="blog-sidebar__widget">
        <h2 class="blog-sidebar__title">Mọi điều cần biết</h2>
        <div class="blog-sidebar__categories">
            <ul>
                @for($i=0; $i<4; $i++)
                    <li><a href="/frontend/blog/category">Iphone</a>
                        <ul>
                            @for($j=0; $j<5; $j++)
                                <li><a href="/blog/category">iPhone 11 Pro Max</a></li>
                            @endfor
                        </ul>
                    </li>
                @endfor
            </ul>
        </div>
    </section>
</aside>
