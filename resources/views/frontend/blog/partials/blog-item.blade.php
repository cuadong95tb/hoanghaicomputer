<article class="blog-item">
    <div class="blog-item__thumb">
        <a href="/frontend/blog/detail">
            <img src="https://storage.googleapis.com/shopdunk-images/tintucshopdunknew/2020/05/25acb1ae-tua-game-thu-thanh-tren-iphone-5-600x315.jpg" alt="">
        </a>
    </div>
    <div class="blog-item__content">
        <h3 class="blog-item__title">
            <a href="/frontend/blog/detail">MacBook Pro 2020 và Surface Book 3: "quái vật" đến từ Apple và Microsoft</a>
        </h3>
        <div class="blog-item__time-cate">
            <span>19:00 ngày 14/05/2020</span>
            <a href="/frontend/blog/detail">iPhone - iOS</a>
        </div>
        <div class="blog-item__desc">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus aliquam asperiores autem commodi cupiditate doloribus eligendi ex in ipsum nihil nisi provident quia sapiente soluta tempora totam unde voluptate, voluptatem.
        </div>
    </div>
</article>
