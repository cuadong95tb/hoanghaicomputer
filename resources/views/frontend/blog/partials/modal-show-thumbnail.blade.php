<div class="modal fade modal-show-thumbnail">
    <div class="modal-dialog modal-dialog-centered">
        <img class="modal-show-thumbnail__img" src="" alt="">
        <button type="button" class="modal-show-thumbnail__close" data-dismiss="modal" aria-label="Close"><i class="fal fa-times"></i></button>
    </div>
</div>
