<article class="blog-item--shortcut">
    <div class="blog-item--shortcut__thumb">
        <a href="/frontend/blog/detail">
            <img src="https://storage.googleapis.com/shopdunk-images/tintucshopdunknew/2020/05/25acb1ae-tua-game-thu-thanh-tren-iphone-5-600x315.jpg" alt="">
        </a>
    </div>
    <div class="blog-item--shortcut__content">
        <h3 class="blog-item--shortcut__title">
            <a href="/frontend/blog/detail">MacBook Pro 2020 và Surface Book 3: "quái vật" đến từ Apple và Microsoft</a>
        </h3>
    </div>
</article>