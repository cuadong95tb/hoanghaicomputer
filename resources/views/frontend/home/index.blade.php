@extends('frontend.layouts.master')
@section('meta')
    <title>Trang chủ</title>
@stop
@section('stylesheet')
    <link rel="stylesheet" href="{{asset('assets/home/index.css')}}">
@stop
@section('content')

    {{--Home Banner--}}
    @include('frontend.home.partials.home-banner')

    {{--Best Sale Products--}}
    @php
    $categories_best_sale = ['iPhone', 'iPad', 'Apple Watch', 'Mac Book'];
    @endphp
    @foreach($categories_best_sale as $cate)
        @include('frontend.home.partials.home-products', [$cate => 'cate'])
    @endforeach

    {{--Promotion Products--}}
    @include('frontend.home.partials.home-products-promotion')

    {{--Slider Customers--}}
    @include('frontend.home.partials.home-customers')

    {{--slider manufacturer--}}
    @include('frontend.home.partials.home-manufacturer')
@stop
@section('script')
    <script src="{{asset('assets/home/index.js')}}"></script>
@stop