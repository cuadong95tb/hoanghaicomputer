<section class="home-banner">
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <div class="home-banner__slider">
                    <div class="slider-for">
                        @for ($i=0; $i<5; $i++)
                        <div class="slider-for__item">
                            <a href="#"><img src="https://shopdunk.com/wp-content/uploads/2020/03/PC-1.jpg" alt=""></a>
                        </div>
                        @endfor
                    </div>
                    <div class="slider-nav">
                        @for ($i=0; $i<5; $i++)
                        <div class="slider-nav__item">
                            <h5>Miễn phí giao hàng tận nhà {{$i}}</h5>
                        </div>
                        @endfor
                    </div>
                </div>
            </div>
            <div class="col-md-5">
                <div class="home-banner__thumbnail">
                    <div class="row">
                        <div class="col-12">
                            <a href="#">
                                <img src="https://shopdunk.com/wp-content/uploads/2020/02/banner-web-sd2.jpg" alt="">
                            </a>
                        </div>
                        <div class="col-6">
                            <a href="">
                                <img src="https://shopdunk.com/wp-content/uploads/2019/11/6-2-1.jpg" alt="">
                            </a>
                        </div>
                        <div class="col-6">
                            <a href="">
                                <img src="https://shopdunk.com/wp-content/uploads/2019/11/5-2-1.jpg" alt="">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>