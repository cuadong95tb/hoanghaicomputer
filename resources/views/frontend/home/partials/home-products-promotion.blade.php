<section class="home-products home-products--promotion">
    <div class="container position-relative">
        <h3 class="title-section"><i class="fad fa-pig"></i>Sản phẩm khuyến mãi</h3>
        <div class="home-products__list home-products--promotion__slider">
            @for($i=0; $i<12; $i++)
                <div class="home-products--promotion__item">
                    @include('frontend.partials.item-product')
                </div>
            @endfor
        </div>
    </div>
</section>