<section class="home-products">
    <div class="container">
        <h3 class="title-section">{{$cate}} bán chạy nhất</h3>
        <div class="home-products__list">
            <div class="row">
                @for($i=0; $i<6; $i++)
                    <div class="col-md-6 col-xl-4 d-flex flex-wrap">
                        @include('frontend.partials.item-product')
                    </div>
                @endfor
            </div>
        </div>
    </div>
</section>