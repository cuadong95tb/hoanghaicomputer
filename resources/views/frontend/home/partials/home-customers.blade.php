<section class="home-customers">
    <div class="container">
        <h3 class="title-section">Khách hàng của chúng tôi</h3>
        <div class="home-customers__slider">
            @for($i=0; $i<6; $i++)
                @include('frontend.partials.customer-feedback')
            @endfor
        </div>
    </div>
</section>