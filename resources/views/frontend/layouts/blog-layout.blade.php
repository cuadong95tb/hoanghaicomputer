<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" href="{{asset('images/icon.png')}}" type="image/gif">
    @yield('meta')
    @yield('stylesheet')
</head>
<body>
@include('frontend.blog.partials.header')
@yield('content')
@include('frontend.partials.footer')
@yield('script')
</body>
</html>