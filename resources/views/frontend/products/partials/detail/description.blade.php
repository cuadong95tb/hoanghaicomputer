<section class="description active-menu-scroll" id="description">
    <div class="text-format">
        {{--content--}}
            <h4>I. Đặc điểm nổi bật của iPhone 11 Pro Max Mỹ (LL/A) 1 Sim</h4>
            <h5>1. Thiết kế đẳng cấp</h5>
            <p>iPhone 11 Pro Max đã được ra mắt vào ngày 11/09/2019 vừa qua, với ngôn ngữ thiết kế không có nhiều thay đổi so với iPhone Xs và iPhone Xs Max. Duy chỉ có logo Apple thì được đặt ở chính giữa mặt sau của máy. Máy vẫn được bọc bởi lớp thép không gỉ. Mặt lưng là cả một tấm kính duy nhất là kính nhám chứ không phải bóng như trên iPhone Xs và Xs Max. iPhone 11 Pro có 4 màu sắc được Apple cho ra mắt: Midnight Green, Space Gray, Silver, Gold.</p>
            <p style="text-align: center"><img class="size-full wp-image-15408 aligncenter" src="https://shopdunk.com/wp-content/uploads/2019/08/ip11-1.jpg" alt=""
                     width="700" height="450" srcset="https://shopdunk.com/wp-content/uploads/2019/08/ip11-1.jpg 700w, https://shopdunk.com/wp-content/uploads/2019/08/ip11-1-300x193.jpg 300w" sizes="(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 984px) 61vw, (max-width: 1362px) 45vw, 600px"></p>
            <h5>2. Camera góc rộng, chế độ Night Mode</h5>
            <p>Camera góc siêu rộng là một bổ sung đáng quan tâm, nó xuất hiện trên cả iPhone 11 và iPhone 11 Pro / Pro Max. Trước đây Apple và nhiều hãng điện thoại khác chủ yếu trang bị camera thường và camera tele cho những chiếc smartphone camera kép mà thôi, chỉ một vài hãng như LG và Samsung là có đưa góc rộng vào. Giờ thì iPhone cũng đã đi theo hướng này, và hãy kỳ vọng rằng trong thời gian tới nhiều mẫu điện thoại Android khác cũng sẽ thực hiện theo từ giá rẻ đến tầm trung.&nbsp;Ống kính góc rộng cho phép bạn chụp cảnh trọn vẹn hơn, nó cũng tạo ra các hiệu ứng ấn tượng hơn nhờ hiệu ứng cong và trường nhìn rộng kể cả khi bạn chỉ chụp người, chụp ảnh đời sống thường ngày.</p>
            <p><img class="size-full wp-image-15391 aligncenter" src="https://shopdunk.com/wp-content/uploads/2019/08/iP-Pro-11-700x450-4.jpg" alt="" width="700" height="450" srcset="https://shopdunk.com/wp-content/uploads/2019/08/iP-Pro-11-700x450-4.jpg 700w, https://shopdunk.com/wp-content/uploads/2019/08/iP-Pro-11-700x450-4-300x193.jpg 300w" sizes="(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 984px) 61vw, (max-width: 1362px) 45vw, 600px"></p>
            <p>Night Mode, chế độ chụp ảnh thiếu sáng, cũng đã có mặt từ lâu trên điện thoại Android nhưng nay iPhone mới có. Nó kết hợp cảm biến mới và thuật toán để làm cho ảnh chụp ban đêm sáng lên đáng kể so với bình thường. Đây cũng là cách mà Google từng làm chúng ta há hốc mồm với chế độ Night Shot của ứng dụng Google Camera.</p>
            <p><img class="size-full wp-image-15394 aligncenter" src="https://shopdunk.com/wp-content/uploads/2019/08/11pro.jpg" alt="" width="700" height="450" srcset="https://shopdunk.com/wp-content/uploads/2019/08/11pro.jpg 700w, https://shopdunk.com/wp-content/uploads/2019/08/11pro-300x193.jpg 300w" sizes="(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 984px) 61vw, (max-width: 1362px) 45vw, 600px"></p>
            <h5>3. Công nghệ màn hình mới</h5>
            <p>Apple nói iPhone 11 là màn hình sáng nhất, đẹp nhất họ từng trang bị cho một chiếc iPhone. Apple mang công nghệ hình ảnh XDR từ chiếc màn hình Apple Pro Display XDR lên iPhone 11 để tăng cường dải tương phản động giúp màu sắc thật hơn, sự khác biệt giữa vùng màu sáng và vùng màu tối rõ rệt hơn. Màn hình của iPhone 11 Pro Max có thể tăng độ sáng lên 800 nit khi bạn dùng ở ngoài đường, không lo bị chói, và nếu cần nó có thể tăng tiếp lên 1200 nit. Màn hình mới của iPhone được Apple gọi là “Super Retina XDR”.</p>
            <p style="text-align: center"><img class="size-full wp-image-15393 aligncenter" src="https://shopdunk.com/wp-content/uploads/2019/08/iP-Pro-11-700x450-6.jpg"
                     alt="" width="700" height="450" srcset="https://shopdunk.com/wp-content/uploads/2019/08/iP-Pro-11-700x450-6.jpg 700w, https://shopdunk.com/wp-content/uploads/2019/08/iP-Pro-11-700x450-6-300x193.jpg 300w" sizes="(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 984px) 61vw, (max-width: 1362px) 45vw, 600px"></p>
            <h5>4. Chip Apple A13 Bionic mạnh mẽ</h5>
            <p>Chip Apple A13 năm nay được sản xuất trên dây chuyền 7nm thế hệ thứ 2. Về cơ bản thì nó không đổi về mặt cấu hình quá nhiều so với năm ngoái, chúng ta vẫn có CPU, GPU và một bộ phận xử lý tác vụ trí tuệ nhân tạo riêng là Neural Engine. Cái mà Apple thay đổi nằm ở tầng bán dẫn, hãng tăng số bóng bán dẫn lên thành 8,5 tỉ bóng, nhiều nhất từ trước đến nay trong số các chip dòng A. Tức là hãng tăng hiệu năng của cả CPU và GPU lên. Apple có đưa ra slide so sánh để khoe rằng A13 Bionic mạnh hơn so với nhiều con Android khác hiện nay từ Huawei cho đến Samsung.</p>
            <p style="text-align: center"><img class="size-full wp-image-15390 aligncenter" src="https://shopdunk.com/wp-content/uploads/2019/08/iP-Pro-11-700x450-2.jpg"
                     alt="" width="700" height="450" srcset="https://shopdunk.com/wp-content/uploads/2019/08/iP-Pro-11-700x450-2.jpg 700w, https://shopdunk.com/wp-content/uploads/2019/08/iP-Pro-11-700x450-2-300x193.jpg 300w" sizes="(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 984px) 61vw, (max-width: 1362px) 45vw, 600px"></p>
            <h5>5. Thời gian dùng pin được cải thiện đáng kể</h5>
            <p>Nhờ việc tối ưu chip, tăng cường các vùng điện thế trên chip để chỉ bật những khu vực nào được sử dụng, tối ưu luôn cả màn hình, mà iPhone 11 Pro có thêm 4 tiếng dùng pin so với iPhone XS hiện nay.</p>
            <p><img class="size-full wp-image-15392 aligncenter" src="https://shopdunk.com/wp-content/uploads/2019/08/iP-Pro-11-700x450-5.jpg" alt="" width="700" height="450" srcset="https://shopdunk.com/wp-content/uploads/2019/08/iP-Pro-11-700x450-5.jpg 700w, https://shopdunk.com/wp-content/uploads/2019/08/iP-Pro-11-700x450-5-300x193.jpg 300w" sizes="(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 984px) 61vw, (max-width: 1362px) 45vw, 600px"></p>
            <h5>6. Sạc nhanh đi kèm điện thoại</h5>
            <p>Lần đầu tiên kể từ khi iPhone ra đời năm 2007, Apple đã nâng cấp cục sạc pin đi kèm trong hộp sản phẩm: iPhone 11 Pro và Pro Max sẽ được bán kèm cục sạc nhanh 18W. Cục sạc này dùng cổng USB-C nên bạn sẽ có một sợi dây USB-C – Lightning trong hộp luôn.</p>
            <p><img class="size-full wp-image-15409 aligncenter" src="https://shopdunk.com/wp-content/uploads/2019/08/ip11-2.jpg" alt="" width="700" height="450" srcset="https://shopdunk.com/wp-content/uploads/2019/08/ip11-2.jpg 700w, https://shopdunk.com/wp-content/uploads/2019/08/ip11-2-300x193.jpg 300w" sizes="(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 984px) 61vw, (max-width: 1362px) 45vw, 600px"></p>
            <h4><strong>II. Mua iPhone 11 Pro Max&nbsp;</strong><strong>giá tốt cùng chế độ bảo hành dài ở đâu?</strong></h4>
            <h5>1. Mua trả góp iPhone 11 Pro Max lãi suất 0 đồng&nbsp;với thẻ tín dụng</h5>
            <ul>
                <li>Không cần duyệt hồ sơ hay cần bất kỳ thủ tục giấy tờ phức tạp, thủ tục nhanh chóng và đơn giản.</li>
                <li>Khách hàng khi tra góp chỉ cần có thẻ tín dụng VISA/MASTER/JCB (loại tiêu trước trả sau – Credit) của một trong 19 ngân hàng hợp tác trên. Riêng VPANK chưa áp dụng thẻ đồng thương hiệu của Fecredit/TIMO.</li>
                <li>Thời gian trả góp linh hoạt từ 3 tháng cho đến 12 tháng cho tất cả các sản phẩm có giá trị từ 3.000.000 đồng trở lên (VIB từ 2000.000 đồng).</li>
                <li>Bạn có thể thanh toán trước 1 phần, trả góp 1 phần giá trị (tùy vào hạn mức thẻ mà bạn có)</li>
                <li>Ngân hàng hợp tác với ShopDunk: Sacombank, Citybank, Eximbank, HSBC, Maritimebank, SHINHAN, Techcombank, VIB, VPBank, SeaBank, Standared Chartered, SCB, Nam Á, FE Credit, OCB, TPBank.</li>
            </ul>
            <h5>2. Nhanh chóng dễ dàng hơn với 4 bước mua iPhone 11 Pro Max tại ShopDunk</h5>
            <ul>
                <li>Không cần phải đến tận nơi mua, không cần phải lo lắng sẽ bị “ăn quả lừa” khi mua hàng không được “nhìn tận mắt, sờ tận tay” hay chuyển tiền rồi mà không nhận được hàng, khách hàng hoàn toàn có thể kiểm tra hàng, đồng ý giao dịch rồi thanh toán với dịch vụ mua hàng ship COD của ShopDunk.</li>
                <li>Với tôn chỉ đặt lợi ích của khách hàng lên hàng đầu, ShopDunk đã hợp tác cùng Viettel Post để triển khai dịch vụ ship COD miễn phí trên toàn quốc. Khách hàng chỉ cần ở nhà, đặt hàng, nhận hàng, kiểm tra và thanh toán là đã có ngay chiếc iPhone sang chảnh trên tay.</li>
            </ul>
            <p>=&gt; Hướng dẫn các bước mua hàng ship COD:</p>
            <ul>
                <li>Bước 1: Chọn sản phẩm bạn đang muốn mua, khách hàng để lại thông tin, nhân viên tư vấn của ShopDunk sẽ gọi điện xác minh thông tin.</li>
                <li>Bước 2:&nbsp;Nhân viên ShopDunk lập phiếu ship COD.</li>
                <li>Bước 3: Giao hàng.</li>
                <li>Bước 4:&nbsp;Kiểm tra hàng khi nhận và thanh toán.</li>
            </ul>
            <p>=&gt; Thông tin chi tiết liên hệ hotline&nbsp;1900.6626&nbsp;hoặc truy cập website&nbsp;<a href="https://shopdunk.com/">shopdunk.com</a></p>
        {{--end content--}}
    </div>
</section>