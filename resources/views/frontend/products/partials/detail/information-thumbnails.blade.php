<section class="information-thumbnails">
    <div class="easyzoom easyzoom--overlay easyzoom--with-thumbnails">
        <a href="https://i.picsum.photos/id/0/1000/1000.jpg">
            <img src="https://i.picsum.photos/id/0/1000/1000.jpg" class="w-100" alt="" />
        </a>
    </div>
    <div class="information-thumbnails__list">
        <div class="row">
            @for($i=0; $i<12; $i++)
            <div class="col-2">
                <a
                        class="{{$i == 0 ? 'active' : ''}}"
                        href="https://i.picsum.photos/id/{{$i}}/1000/1000.jpg"
                        data-standard="https://i.picsum.photos/id/{{$i}}/1000/1000.jpg">
                    <img src="https://i.picsum.photos/id/{{$i}}/1000/1000.jpg" alt="">
                </a>
            </div>
            @endfor
        </div>
    </div>
</section>
