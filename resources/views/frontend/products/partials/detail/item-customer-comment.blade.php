<article class="customer-comment-item">
    <div class="customer-comment-item__rating">
        <span class="star-rating">
            <span style="width: 80%"></span>
        </span>
    </div>
    <div class="customer-comment-item__auth">
        Bởi: <strong>Phúc</strong>  vào ngày 11/04/2020
    </div>
    <div class="customer-comment-item__content">
        <div class="text-format">
            Sau gần 1 tháng sử dụng samsung s20 plus này tôi cảm nhận được: Cầm gọn tay khi sử dụng,lướt phím cực mướt. Nghe,gọi chất lượng âm thanh tốt,loa k rè. Máy máy ư? Quá ok (Mặc dù chưa khai thác hết chức năng của camera).
        </div>
    </div>
    <div class="customer-comment-item__like">
        <button class="button-like">18</button>
    </div>
</article>