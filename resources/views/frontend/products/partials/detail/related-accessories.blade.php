<section class="related-accessories">
    <h3 class="related-accessories__title">Phụ kiện hay mua cùng</h3>
    <div class="related-accessories__slider">
        @for($i=0; $i<12; $i++)
        <article class="related-accessories__item">
            <a href="/frontend/products/detail">
            <div class="accessory">
                <div class="accessory__thumb">
                    <img src="https://i.picsum.photos/id/20/500/500.jpg" alt="">
                </div>
                <div class="accessory__content">
                    <h3 class="accessory__title">Apple Magic Keyboard iPad Pro 12.9 inch 2020 - Chính hãng (Full VAT)</h3>
                    <div class="accessory__price">
                        <div class="price has-sale">
                            <span class="price__original">28.090.000đ</span>
                            <span class="price__sale">25.090.000đ</span>
                        </div>
                    </div>
                    <div class="accessory__reviews">
                        <span class="star-rating">
                            <span style="width: 90%"></span>
                        </span>
                        <small class="d-inline-block">(42 đánh giá)</small>
                    </div>
                </div>
            </div>
            </a>
        </article>
        @endfor
    </div>
</section>
