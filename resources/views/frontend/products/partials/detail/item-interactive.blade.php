<div class="item-interactive">
    <div class="item-interactive__comment">
        @include('frontend.products.partials.detail.user-interactive')
    </div>
    <div class="item-interactive__actions">
        <a href="#" class="action-reply"><i class="fad fa-reply-all"></i> Trả lời</a>
        <a href="#" class="action-hide"><i class="fas fa-minus-circle"></i> Ẩn</a>
        <a href="#" class="action-delete"><i class="fas fa-times-circle"></i> Xóa</a>
    </div>
    <div class="item-interactive__reply">
        @for($i=0; $i<3; $i++)
        @include('frontend.products.partials.detail.user-interactive')
        @endfor
    </div>
    <div class="item-interactive__add-reply">
        @include("frontend.products.partials.detail.form-add-interactive")
    </div>
</div>
