<section class="questions-and-answers active-menu-scroll" id="questions-and-answers">
    <h3 class="questions-and-answers__title">Hỏi đáp về Samsung Galaxy S20+</h3>
    <div class="questions-and-answers__main">
        <div class="questions-and-answers__add-question">
            @include("frontend.products.partials.detail.form-add-interactive")
        </div>
        <div class="questions-and-answers__list">
            @for ($i=0; $i<5; $i++)
                @include("frontend.products.partials.detail.item-interactive")
            @endfor
        </div>
        <div class="questions-and-answers__pagination">
            @include('frontend.partials.pagination')
        </div>
    </div>
    <div class="modal fade modal-complete-send-question">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Hoàn thành gửi câu hỏi</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i class="fal fa-times"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="" class="form-complete-send-question" method="">
                        <h6><em>Vui lòng nhập thông tin để hoàn thành gửi câu hỏi!</em></h6>
                        <div class="form-group">
                            <div class="form-group__row">
                                <label>Họ và tên: <span>*</span></label>
                                <input
                                        id="name-user-send-question"
                                        class="form-control"
                                        type="text"
                                        maxlength="255"
                                        placeholder="Nhập họ tên của bạn"
                                >
                            </div>
                            <div class="form-group__alert">
                                <div data-error="name-user-send-question" class="alert-error"></div>
                            </div>
                        </div>
                        <p class="mb-1"><small><em>Để nhận thông báo khi có trả lời. Hãy nhập email và số điện thoại (Không bắt buộc)</em></small></p>
                        <div class="form-group">
                            <div class="form-group__row">
                                <label>Số điện thoại:</label>
                                <input
                                        id="phone-user-send-question"
                                        class="form-control"
                                        type="tel"
                                        maxlength="255"
                                        placeholder="Nhập số điện thoại của bạn"
                                >
                            </div>
                            <div class="form-group__alert">
                                <div data-error="phone-user-send-question" class="alert-error"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-group__row">
                                <label>Email:</label>
                                <input
                                        id="email-user-send-question"
                                        class="form-control"
                                        type="email"
                                        maxlength="255"
                                        placeholder="Nhập Email của bạn"
                                >
                            </div>
                            <div class="form-group__alert">
                                <div data-error="email-user-send-question" class="alert-error"></div>
                            </div>
                        </div>
                        <div class="text-center">
                            <button type="submit" class="btn btn-danger">Gửi nhận xét</button>
                        </div>
                    </form>
                </div>
                <div class="modal-footer justify-content-center">
                    <div class="text-center">
                        <h6>Hoặc gửi nhanh bằng cách</h6>
                        <button class="btn btn-facebook">
                            <i class="fab fa-facebook-square"></i> Đăng nhập với facebook
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>