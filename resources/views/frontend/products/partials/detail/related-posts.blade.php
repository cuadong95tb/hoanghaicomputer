<section class="related-posts">
    <h3 class="related-posts__title">Bài viết liên quan</h3>
    <div class="related-posts__list">
        @for($i=0; $i<5; $i++)
            @include('frontend.blog.partials.blog-item-shortcut')
        @endfor
    </div>
    <div class="related-posts__view-more">
        <a href="/frontend/blog/category">Xem tất cả <i class="fal fa-angle-right"></i></a>
    </div>
</section>