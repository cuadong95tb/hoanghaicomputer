<section class="reviews-and-comments active-menu-scroll" id="reviews-and-comments">
    <h3 class="reviews-and-comments__title">Đánh giá & nhận xét về Samsung Galaxy S20+</h3>
    <div class="reviews-and-comments__main">
        <div class="reviews-statistic">
            <div class="reviews-statistic__result">
                <h3>3,8/5</h3>
                <span class="star-rating">
                    <span style="width: 90%"></span>
                </span>
                <p>83 đánh giá & nhận xét</p>
            </div>
            <div class="reviews-statistic__rule">
                <table>
                    <tr>
                        <td>5 sao</td>
                        <td>
                            <span class="rule">
                                <span style="width: 55%"></span>
                            </span>
                        </td>
                        <td>38</td>
                    </tr>
                    <tr>
                        <td>4 sao</td>
                        <td>
                            <span class="rule">
                                <span style="width: 20%"></span>
                            </span>
                        </td>
                        <td>12</td>
                    </tr>
                    <tr>
                        <td>3 sao</td>
                        <td>
                            <span class="rule">
                                <span style="width: 12%"></span>
                            </span>
                        </td>
                        <td>11</td>
                    </tr>
                    <tr>
                        <td>2 sao</td>
                        <td>
                            <span class="rule">
                                <span class="bg-warning" style="width: 8%"></span>
                            </span>
                        </td>
                        <td>11</td>
                    </tr>
                    <tr>
                        <td>1 sao</td>
                        <td>
                            <span class="rule">
                                <span class="bg-danger" style="width: 5%"></span>
                            </span>
                        </td>
                        <td>5</td>
                    </tr>
                </table>
            </div>
            <div class="reviews-statistic__add-review">
                <h3>Bạn đã dùng sản phẩm này?</h3>
                <a href="#form-add-review" class="open-form-review">Gửi đánh giá của bạn</a>
            </div>
        </div>
        <div id="form-add-review" class="add-review">
            <h3 class="add-review__title">Gửi nhận xét của bạn</h3>
            <div class="add-review__box">
                <form class="form-add-review" action="">
                    <div class="form-group mb-2">
                        <h6 class="m-0">Bạn đánh giá sản phẩm này đạt bao nhiêu sao?</h6>
                        @include('frontend.partials.select-star-rating')
                        <div class="form-group__alert">
                            <div data-error="radio-rating" class="alert-error"></div>
                        </div>
                    </div>
                    <div class="form-group mb-2">
                        <textarea
                                id="txt-review-content"
                                class="form-control"
                                name=""
                                rows="3"
                                minlength="3"
                                placeholder="Bạn có khuyên người khác mua sản phẩm này không? Tại sao?"
                        ></textarea>
                        <div class="form-group__alert">
                            <div data-error="txt-review-content" class="alert-error"></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <small class="d-inline-block mb-2"><em>Mỗi đánh giá có ích thường dài 100 kí tự trở lên</em></small>
                        </div>
                        <div class="col-sm-6 text-right">
                            <button class="close-form-review btn btn-light" type="reset">Hủy</button>
                            <button class="btn btn-danger" type="submit">Gửi đánh giá</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="customer-comments">
            <h3 class="customer-comments__title">Khách hàng nhận xét (<strong>11</strong>)</h3>
            <div class="customer-comments__list">
                @for($i=0; $i<5; $i++)
                    @include('frontend.products.partials.detail.item-customer-comment')
                @endfor
            </div>
            <div class="text-center mt-3">
                <button class="customer-comments__view-more">Xem tất cả <strong>6</strong> nhận xét</button>
            </div>
        </div>
    </div>
    <div class="modal fade modal-complete-send-review">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Hoàn thành gửi nhận xét</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i class="fal fa-times"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="" class="form-complete-send-review">
                        <h6><em>Vui lòng nhập thông tin để hoàn thành gửi đánh giá!</em></h6>
                        <div class="form-group">
                            <div class="form-group__row">
                                <label>Họ và tên: <span>*</span></label>
                                <input
                                        id="name-user-send-review"
                                        class="form-control"
                                        type="text"
                                        maxlength="255"
                                        placeholder="Nhập họ tên của bạn"
                                >
                            </div>
                            <div class="form-group__alert">
                                <div data-error="name-user-send-review" class="alert-error"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-group__row">
                                <label>Số điện thoại: <span>*</span></label>
                                <input
                                        id="phone-user-send-review"
                                        class="form-control"
                                        type="tel"
                                        maxlength="255"
                                        placeholder="Nhập số điện thoại của bạn"
                                >
                            </div>
                            <div class="form-group__alert">
                                <div data-error="phone-user-send-review" class="alert-error"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-group__row">
                                <label>Email:</label>
                                <input
                                        id="email-user-send-review"
                                        class="form-control"
                                        type="email"
                                        maxlength="255"
                                        placeholder="Nhập Email của bạn"
                                >
                            </div>
                            <div class="form-group__alert">
                                <div data-error="email-user-send-review" class="alert-error"></div>
                            </div>
                        </div>
                        <div class="text-center">
                            <button type="submit" class="btn btn-danger">Gửi nhận xét</button>
                        </div>
                    </form>
                </div>
                <div class="modal-footer justify-content-center">
                    <div class="text-center">
                        <h6>Hoặc gửi nhanh bằng cách</h6>
                        <button class="btn btn-facebook">
                            <i class="fab fa-facebook-square"></i> Đăng nhập với facebook
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>