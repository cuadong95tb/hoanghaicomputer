<section class="specifications">
    <h3 class="specifications__title">Thông số kĩ thuật</h3>
    <div class="specifications__content">
        <table class="table">
            <tr>
                <td>Màn hình:</td>
                <td>6.7 inchs, 2K+, 2K+ (1440 x 3200 Pixels)</td>
            </tr>
            <tr>
                <td>Camera trước:</td>
                <td>10MP</td>
            </tr>
            <tr>
                <td>Camera sau:</td>
                <td>Chính 12 MP &amp; Phụ 64 MP, 12 MP, TOF 3D</td>
            </tr>
            <tr>
                <td>RAM:</td>
                <td>8 GB</td>
            </tr>
            <tr>
                <td>Bộ nhớ trong:</td>
                <td>128 GB</td>
            </tr>
            <tr>
                <td>CPU:</td>
                <td>Exynos 990, 8, 2 nhân 2.73 GHz, 2 nhân 2.6 GHz &amp; 4 nhân 2.0 GHz</td>
            </tr>
            <tr>
                <td>GPU:</td>
                <td>Matr-G77 MP11</td>
            </tr>
            <tr>
                <td>Dung lượng pin:</td>
                <td>4500 mAh</td>
            </tr>
            <tr>
                <td>Hệ điều hành:</td>
                <td>Android 10.0</td>
            </tr>
            <tr>
                <td>Thẻ SIM:</td>
                <td>2 SIM Nano (SIM 2 chung khe thẻ nhớ), 2 Sim</td>
            </tr>
            <tr>
                <td>Xuất xứ:</td>
                <td>Việt Nam</td>
            </tr>
            <tr>
                <td>Năm sản xuất:</td>
                <td>2020</td>
            </tr>
        </table>
    </div>
    <div class="specifications__view-more">
        <button data-toggle="modal" data-target="#modal-specifications">Xem cấu hình chi tiết</button>
    </div>
    <div class="specifications__modal modal fade"
         id="modal-specifications"
         tabindex="-1"
         role="dialog"
         aria-hidden="true"
    >
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Thông số kĩ thuật chi tiết</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-td="Close">
                        <i class="fal fa-times"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <table class="table">
                        <tr>
                            <td colspan="2">
                                <h6 class="table__title">Màn hình</h6>
                            </td>
                        </tr>
                        <tr>
                            <td>Công nghệ màn hình:</td>
                            <td>Dynamic AMOLED 2X</td>
                        </tr>
                        <tr>
                            <td>Màu màn hình:</td>
                            <td>16 triệu màu</td>
                        </tr>
                        <tr>
                            <td>Chuẩn màn hình:</td>
                            <td>2K+</td>
                        </tr>
                        <tr>
                            <td>Độ phân giải màn hình:</td>
                            <td>2K+ (1440 x 3200 Pixels)</td>
                        </tr>
                        <tr>
                            <td>Màn hình:</td>
                            <td>6.7 inchs</td>
                        </tr>
                        <tr>
                            <td>Mặt kính màn hình:</td>
                            <td>Kính cường lực Corning Gorilla Glass 6</td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <h6 class="table__title">Camera Trước</h6>
                            </td>
                        </tr>
                        <tr>
                            <td>Độ phân giải:</td>
                            <td>10MP</td>
                        </tr>
                        <tr>
                            <td>Thông tin khác:</td>
                            <td>HDR, Tự động lấy nét (AF), Quay video Full HD, Làm đẹp (Beautify), Nhận diện khuôn mặt, Chụp bằng cử chỉ, Flash màn hình, Nhãn dán (AR Stickers), Quay phim 4K, Xoá phông</td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <h6 class="table__title">Camera Sau</h6>
                            </td>
                        </tr>
                        <tr>
                            <td>Độ phân giải:</td>
                            <td>Chính 12 MP Phụ 64 MP, 12 MP, TOF 3D</td>
                        </tr>
                        <tr>
                            <td>Quay phim:</td>
                            <td>Quay phim HD 720p@960fps, Quay phim FullHD 1080p@30fps, Quay phim FullHD 1080p@60fps, Quay phim FullHD 1080p@240fps, Quay phim 4K 2160p@30fps, Quay phim 4K 2160p@60fps, Quay phim 8K 4320p@24fps</td>
                        </tr>
                        <tr>
                            <td>Đèn Flash:</td>
                            <td>Có</td>
                        </tr>
                        <tr>
                            <td>Chụp ảnh nâng cao:</td>
                            <td>Góc siêu rộng , Zoom quang học, Xoá phông,Trôi nhanh thời gian , Ban đêm , A.I Camera, Điều chỉnh khẩu độ, Quay siêu chậm , Tự động lấy nét (AF), Chạm lấy nét, Nhận diện khuôn mặt, HDR, Toàn cảnh , Chống rung quang học, Chuyên nghiệp</td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <h6 class="table__title">Cấu hình phần cứng</h6>
                            </td>
                        </tr>
                        <tr>
                            <td>Tốc độ CPU:</td>
                            <td>2 nhân 2.73 GHz, 2 nhân 2.6 GHz 4 nhân 2.0 GHz</td>
                        </tr>
                        <tr>
                            <td>Số nhân:</td>
                            <td>8</td>
                        </tr>
                        <tr>
                            <td>Chipset:</td>
                            <td>Exynos 990</td>
                        </tr>
                        <tr>
                            <td>RAM:</td>
                            <td>8 GB</td>
                        </tr>
                        <tr>
                            <td>Chip đồ họa (GPU):</td>
                            <td>Matr-G77 MP11</td>
                        </tr>
                        <tr>
                            <td data-id="1345">Cảm biến:</td>
                            <td>Mở khoá vân tay dưới màn hình</td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <h6 class="table__title">Bộ nhớ Lưu trữ</h6>
                            </td>
                        </tr>
                        <tr>
                            <td data-id="23">Danh bạ lưu trữ:</td>
                            <td>Không giới hạn</td>
                        </tr>
                        <tr>
                            <td>ROM:</td>
                            <td>128 GB</td>
                        </tr>
                        <tr>
                            <td>Bộ nhớ còn lại:</td>
                            <td>Đang cập nhật</td>
                        </tr>
                        <tr>
                            <td>Thẻ nhớ ngoài:</td>
                            <td>MicroSD</td>
                        </tr>
                        <tr>
                            <td>Hỗ trợ thẻ nhớ tối đa:</td>
                            <td>Tối đa 1TB</td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <h6 class="table__title">Thiết kế Trọng lượng</h6>
                            </td>
                        </tr>
                        <tr>
                            <td>Kiểu dáng:</td>
                            <td>Nguyên Khối</td>
                        </tr>
                        <tr>
                            <td>Chất liệu:</td>
                            <td>Khung kim loại Mặt lưng kính cường lực</td>
                        </tr>
                        <tr>
                            <td>Kích thước:</td>
                            <td>Dài 161.9 mm - Ngang 73.7 mm - Dày 7.8 mm</td>
                        </tr>
                        <tr>
                            <td>Trọng lượng:</td>
                            <td>188 g</td>
                        </tr>
                        <tr>
                            <td>Khả năng chống nước:</td>
                            <td>Có</td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <h6 class="table__title">Thông tin pin</h6>
                            </td>
                        </tr>
                        <tr>
                            <td>Loại pin:</td>
                            <td>Pin chuẩn tr-Ion</td>
                        </tr>
                        <tr>
                            <td>Dung lượng pin:</td>
                            <td>4500 mAh</td>
                        </tr>
                        <tr>
                            <td>Pin có thể tháo rời:</td>
                            <td>Không</td>
                        </tr>
                        <tr>
                            <td>Chế độ sạc nhanh:</td>
                            <td>Có</td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <h6 class="table__title">Kết nối Cổng giao tiếp</h6>
                            </td>
                        </tr>
                        <tr>
                            <td>Loại SIM:</td>
                            <td>2 SIM Nano (SIM 2 chung khe thẻ nhớ)</td>
                        </tr>
                        <tr>
                            <td>Khe cắm sim:</td>
                            <td>2</td>
                        </tr>
                        <tr>
                            <td>Wifi:</td>
                            <td>Dual-band, Wi-Fi 802.11 a/b/g/n/ac/ax, Wi-Fi Direct, Wi-Fi hotspot</td>
                        </tr>
                        <tr>
                            <td>GPS:</td>
                            <td>BDS, A-GPS, GLONASS</td>
                        </tr>
                        <tr>
                            <td>Bluetooth:</td>
                            <td>LE, A2DP, apt-X, v5.0</td>
                        </tr>
                        <tr>
                            <td>GPRS/EDGE:</td>
                            <td>Có</td>
                        </tr>
                        <tr>
                            <td>NFC:</td>
                            <td>Có</td>
                        </tr>
                        <tr>
                            <td>Cổng sạc:</td>
                            <td><a target="_blank" href="https://fptshop.com.vn/tin-tuc/danh-gia/usb-type-c-la-gi-tim-hieu-usb-type-c-57333" title="USB Type-C">USB Type-C</a></td>
                        </tr>
                        <tr>
                            <td>Jack (Input &amp; Output):</td>
                            <td>USB Type C</td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <h6 class="table__title">Giải trí Ứng dụng</h6>
                            </td>
                        </tr>
                        <tr>
                            <td>Xem phim:</td>
                            <td>Có</td>
                        </tr>
                        <tr>
                            <td>Nghe nhạc:</td>
                            <td>Có</td>
                        </tr>
                        <tr>
                            <td>Ghi âm:</td>
                            <td>Có</td>
                        </tr>
                        <tr>
                            <td>FM radio:</td>
                            <td>Có</td>
                        </tr>
                        <tr>
                            <td>Đèn pin:</td>
                            <td>Có</td>
                        </tr>
                        <tr>
                            <td>Chức năng khác:</td>
                            <td>Thu nhỏ màn hình sử dụng một tay, Chuẩn Kháng nước, Chuẩn kháng bụi, Chạm 2 lần sáng màn hình, Sạc pin nhanh, Màn hình luôn hiển thị AOD, Trợ lý ảo Samsung Bixby, Samsung DeX, Nhân bản ứng dụng, Samsung Pay, Âm thanh AKG, Dolby Audio™</td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <h6 class="table__title">Bảo hành</h6>
                            </td>
                        </tr>
                        <tr>
                            <td>Thời gian bảo hành:</td>
                            <td>12 Tháng</td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <h6 class="table__title">Thông tin hàng hóa</h6>
                            </td>
                        </tr>
                        <tr>
                            <td>Xuất xứ:</td>
                            <td>Việt Nam</td>
                        </tr>
                        <tr>
                            <td>Năm sản xuất:</td>
                            <td>2020</td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <h6 class="table__title">Thông tin dòng sản phẩm</h6>
                            </td>
                        </tr>
                        <tr>
                            <td>Model Series:</td>
                            <td>Galaxy S</td>
                        </tr>
                        <tr>
                            <td>
                                <h6 class="table__title">Hệ điều hành</h6>
                            </td>
                        </tr>
                        <tr>
                            <td>Hệ điều hành:</td>
                            <td>Android 10.0</td>
                        </tr>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Đóng</button>
                </div>
            </div>
        </div>
    </div>
</section>