<aside class="information-sidebar">
    <div class="row">
        <div class="col-sm-6 col-lg-12">
            <h3 class="information-sidebar__title">Trong hộp có:</h3>
            <ul class="information-sidebar__list">
                <li><i class="fal fa-box-open"></i>
                    <a data-fancybox="product-box" data-caption="Máy" href="https://i.picsum.photos/id/20/1000/1000.jpg">Máy</a>,
                    <a data-fancybox="product-box" data-caption="Cáp" href="https://i.picsum.photos/id/21/1000/1000.jpg">Cáp</a>,
                    <a data-fancybox="product-box" data-caption="Sạc" href="https://i.picsum.photos/id/22/1000/1000.jpg">Sạc</a>,
                    <a data-fancybox="product-box" data-caption="Tai nghe" href="https://i.picsum.photos/id/23/1000/1000.jpg">Tai nghe</a>,
                    <a data-fancybox="product-box" data-caption="Đệm tai dự phòng" href="https://i.picsum.photos/id/24/1000/1000.jpg">Đệm tai dự phòng</a>,
                    <a data-fancybox="product-box" data-caption="Ốp lưng" href="https://i.picsum.photos/id/25/1000/1000.jpg">Ốp lưng</a>,
                    <a data-fancybox="product-box" data-caption="Sách hướng dẫn sử dụng" href="https://i.picsum.photos/id/26/1000/1000.jpg">Sách hướng dẫn sử dụng</a>,
                    <a data-fancybox="product-box" data-caption="Cây lấy sim" href="https://i.picsum.photos/id/27/1000/1000.jpg">Cây lấy sim</a>
                </li>
            </ul>
        </div>
        <div class="col-sm-6 col-lg-12">
            <h3 class="information-sidebar__title">Chúng tôi cam kết:</h3>
            <ul class="information-sidebar__list">
                <li><i class="fal fa-award"></i>
                    Hàng chính hãng
                </li>
                <li><i class="fal fa-shield-check"></i>
                    Bảo hành 12 Tháng chính hãng
                </li>
                <li><i class="fal fa-shuttle-van"></i>
                    Giao hàng miễn phí toàn quốc trong 60 phút
                </li>
                <li><i class="fal fa-map-marker-alt"></i>
                    Bảo hành nhanh tại Hoàng Hải Computer trên toàn quốc
                </li>
            </ul>
        </div>
    </div>
</aside>