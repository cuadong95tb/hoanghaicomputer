<section class="gallery active-menu-scroll" id="gallery">
    <h3 class="gallery__title">Hình ảnh thực tế <small>(15 ảnh)</small></h3>
    <div class="gallery__slider">
        @for($i=50; $i<75; $i++)
       <div class="gallery__item">
           <a href="https://i.picsum.photos/id/{{$i}}/500/300.jpg" data-fancybox="product-gallery">
               <img src="https://i.picsum.photos/id/{{$i}}/500/300.jpg" alt="">
           </a>
       </div>
        @endfor
    </div>
</section>