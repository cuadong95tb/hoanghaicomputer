<section class="information-actions">
    <div class="information-actions__price">
        <div class="price">
            <p><span class="price__sale">27.850.000đ <small>(20.000¥)</small></span></p>
            <p>Giá gốc: <span class="price__original">27.850.000đ <small>(20.000¥)</small></span></p>
            <p>Bạn tiết kiệm: <span class="price__saving">27.850.000đ <small>(20.000¥)</small></span></p>
        </div>
    </div>
    <div class="information-actions__color">
        <span class="text-uppercase mr-2">Chọn màu:</span>
        <p class="color-board choose-color">
            <label class="active" style="background: red"></label>
            <label style="background: gold"></label>
            <label style="background: gray"></label>
            <label style="background: black"></label>
            <label style="background: orange"></label>
        </p>
    </div>
    <div class="information-actions__capacity">
        <span class="text-uppercase mr-2">Dung lượng:</span>
        <p class="capacity-product choose-capacity">
            <a href=""><label class="active">16GB</label></a>
            <a href=""><label>32GB</label></a>
            <a href=""><label>64GB</label></a>
            <a href=""><label>128GB</label></a>
            <a href=""><label>256GB</label></a>
        </p>
    </div>
    <div class="information-actions__promotion">
        <div class="promotion">
            <h3 class="promotion__title"><i class="fad fa-gifts"></i> Quà khuyến mại</h3>
            <div class="promotion__content text-format">
                <ul>
                    <li>BẢO HÀNH 24 THÁNG</li>
                    <li>Trả góp 0% lãi suất thẻ tín dụng</li>
                    <li>Giảm giá 20% (Cường lực, Ốp lưng)</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="information-actions__add-to-cart">
        <button class="button-add-to-cart">Mua hàng <small>Giao hàng tận nơi hoặc tại cửa hàng</small></button>
    </div>
</section>
