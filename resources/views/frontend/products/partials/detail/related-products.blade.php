<section class="related-products">
    <h3 class="related-products__title">Sản phẩm tương đương</h3>
    <div class="related-products__list">
        @for($i=0; $i<4; $i++)
            <article class="related-product-item">
                <div class="related-product-item__thumb">
                    <a href="/frontend/products/detail">
                        <img src="https://images.fpt.shop/unsafe/fit-in/80x80/filters:quality(90):fill(white)/cdn.fptshop.com.vn/Uploads/Originals/2019/9/11/637037651956109900_11-chung.png" alt="">
                    </a>
                </div>
                <div class="related-product-item__content">
                    <h6 class="related-product-item__title">
                        <a href="/frontend/products/detail">
                            iPhone 11 Pro Max chính hãng Apple xuất xứ Mỹ (LL/A) 1 Sim (Full VAT)
                        </a>
                    </h6>
                    <div class="related-product-item__price">
                        <div class="price has-sale">
                            <span class="price__original">28.090.000đ</span>
                            <span class="price__sale">25.090.000đ</span>
                        </div>
                    </div>
                    <div class="related-product-item__desc text-format">
                        <p>
                            <strong>Màn hình:</strong> 2K+ (1440 x 3200 Pixels) 6.2 inchs <br>
                            <strong>Camera:</strong> Chính 12 MP & Phụ 64 MP, 12 MP 10MP <br>
                            <strong>Pin:</strong> 4000 mAh <br>
                            <strong>RAM:</strong> 8 GB <br>
                            <strong>CPU:</strong> Exynos 990 <br>
                            <strong>HĐH:</strong> Android 10.0
                        </p>
                    </div>
                </div>
            </article>
        @endfor
    </div>
</section>
