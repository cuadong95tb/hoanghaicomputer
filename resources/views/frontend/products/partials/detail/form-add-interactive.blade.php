<form class="form-add-interactive" action="">
    <div class="form-group mb-2">
        <textarea
                rows="3"
                minlength="3"
                class="txt-interactive-content form-control"
                placeholder="Viết bình luận của bạn (Vui lòng gõ tiếng Việt có dấu)"
        ></textarea>
        <div class="alert-error"></div>
    </div>
    <div class="text-right">
        <button class="btn btn-danger">Gửi câu hỏi</button>
    </div>
</form>