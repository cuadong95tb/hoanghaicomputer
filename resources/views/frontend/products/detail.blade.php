@extends('frontend.layouts.master')
@section('meta')
    <title>Chi tiết sản phẩm</title>
@stop
@section('stylesheet')
    <link rel="stylesheet" href="{{asset('assets/products/detail.css')}}">
@stop
@section('content')
    <main class="product-detail-page">
        <div class="container">
            @include('frontend.products.partials.detail.breadcrumb')
            <div class="product-detail-page__content">
                <div class="row">
                    <div class="col-12">
                        <div class="box-information">
                            <div class="row">
                                <div class="col-12">
                                    @include('frontend.products.partials.detail.information-head')
                                </div>
                                <div class="col-md-6 col-lg-4">
                                    @include('frontend.products.partials.detail.information-thumbnails')
                                </div>
                                <div class="col-md-6 col-lg-5">
                                    @include('frontend.products.partials.detail.information-actions')
                                </div>
                                <div class="col-lg-3 d-flex">
                                    @include('frontend.products.partials.detail.information-sidebar')
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="box-accessories">
                            @include('frontend.products.partials.detail.related-accessories')
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <div class="box-interactive">
                            <nav class="box-interactive__nav">
                                <ul>
                                    <li class="active">
                                        <a href="#description">Đặc điểm nổi bật</a>
                                    </li>
                                    <li>
                                        <a href="#reviews-and-comments">Đánh giá & nhận xét</a>
                                    </li>
                                    <li>
                                        <a href="#questions-and-answers">Hỏi đáp</a>
                                    </li>
                                    <li>
                                        <a href="#gallery">Hình ảnh</a>
                                    </li>
                                </ul>
                            </nav>
                            @include('frontend.products.partials.detail.description')
                            @include('frontend.products.partials.detail.reviews-and-comments')
                            @include('frontend.products.partials.detail.questions-and-answers')
                            @include('frontend.products.partials.detail.gallery')
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="box-information-more">
                            <div class="row">
                                <div class="col-md-6 col-lg-12 d-flex">
                                    @include('frontend.products.partials.detail.specifications')
                                </div>
                                <div class="col-md-6 col-lg-12 d-flex">
                                    @include('frontend.products.partials.detail.related-posts')
                                </div>
                                <div class="col-12">
                                    @include('frontend.products.partials.detail.related-products')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@stop
@section('script')
    <script src="{{asset('assets/products/detail.js')}}"></script>
@stop