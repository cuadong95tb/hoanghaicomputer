@extends('frontend.layouts.master')
@section('meta')
    <title>Danh mục sản phẩm</title>
@stop
@section('stylesheet')
    <link rel="stylesheet" href="{{asset('assets/products/category.css')}}">
@stop
@section('content')
    <section class="category-product-page">
        <div class="container">
            <h1 class="title-category">iPhone 12 – 12 Pro – 12 Pro Max </h1>
            <div class="list-products">
                <div class="row">
                    @for($i=0; $i<12; $i++)
                    <div class="col-md-6 col-xl-4 d-flex flex-wrap">
                        @include("frontend.partials.item-product")
                    </div>
                    @endfor
                </div>
            </div>
            <div class="detail-category">
                <div class="row">
                    <div class="col-lg-10 offset-lg-1">
                        <div class="text-format text-justify">
                            {{--content--}}
                            <p><b>Top những mẫu iPad nên mua nhất hiện nay 2019</b></p>
                            <p><b>1. iPad Mini 5</b></p>
                            <p>Kể từ khi ra mắt, dòng sản phẩm iPad mini luôn được người tiêu dùng Việt ưa chuộng nhờ kích cỡ nhỏ nhẹ gọn gàng và mức giá hấp dẫn so với những dòng iPad khác. Sau nhiều năm vắng bóng, giờ đây iPad mini series đã chính thức trở lại với phiên bản iPad mini 5 chạy chip A12 Bionic vô cùng mạnh mẽ. Đặc biệt hơn, phiên bản Wi-Fi 64GB có giá thành hấp dẫn sẽ giúp bạn đến gần hơn bao giờ hết với mong muốn sở hữu một chiếc iPad mini 5.</p>
                            <p><img class="size-full wp-image-2924 aligncenter" src="https://shopdunk.com/wp-content/uploads/2016/11/ipad-mini-4-wifi-den-1.png" alt="" ></p>
                            <p><b>2. iPad Pro</b></p>
                            <p>iPad Pro là chiếc iPad mới nhất của Apple. Đây là iPad lớn nhất với màn hình 12,9-inch.&nbsp;iPad Pro dùng chip A9X mới nhất của Apple. iPad Pro cũng là thiết bị iOS chạy nhanh nhất hiện nay.<br>
                                Ngoài ra iPad Pro còn cung cấp nhiều tính năng chưa từng xuất hiện trên những dòng iPad khác.</p>
                            <p><img class="size-full wp-image-15063 aligncenter" src="https://shopdunk.com/wp-content/uploads/2019/01/12-5.jpg" alt=""></p>
                            <p>iPad Pro còn có kèm theo bút Apple Pencil, giúp các bạn dễ dàng sử dụng cảm ứng và nhạy hơn.<br>
                                Màn hình của iPad Pro có khả năng phát hiện vị trí, lực nhấn và độ nghiêng của bút Apple Pencil, quét vị trí của bút 240/giây, gấp đôi tốc độ của các iPad khác.<br>
                                Với iPad Pro thì bạn sẽ không cần tới bàn phím ngoài kết nối qua Bluetooth vì đã có cổng kết nối Smart Connector để chia sẻ dữ liệu và năng lượng.<br>
                                Nếu bạn là người dùng iPad cho công việc, thì nên sử dụng iPad Pro sẽ rất thích hợp nhé</p>
                            <p><b>2. iPad Air 2<br>
                                </b><br>
                                iPad Air 2 sở hữu chip ba nhân xử lý Apple A8X và RAM 2GB. Nhờ vậy mà iPad Air 2 chạy nhanh hơn, chuyển đổi giữa các ứng dụng cũng rất nhanh và mượt.<br>
                                Màn hình của iPad Air 2 có độ phân giải 2.048 x 1.536 pixel, được phủ một lớp chống phản chiếu lên kính màn hình dùng để chống chói.<br>
                                iPad Air 2 khá mỏng và nhẹ chỉ 437- 444 gram mà thôi.</p>
                            <p><img class="size-full wp-image-7186 aligncenter" src="https://shopdunk.com/wp-content/uploads/2017/07/bao-da-ban-phim-ipad-air2-min-3.jpg" alt=""></p>
                            {{--end content--}}
                        </div>
                    </div>
                </div>
                <a href="" class="detail-category__view-more">Xem thêm <i class="fal fa-angle-double-down"></i></a>
            </div>
        </div>
    </section>
@stop
@section('script')
    <script src="{{asset('assets/products/category.js')}}"></script>
@stop