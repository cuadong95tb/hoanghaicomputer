<article class="product-item">
    <a href="/frontend/products/detail" title="iPhone 11 Pro Max chính hãng Apple xuất xứ Mỹ (LL/A) 1 Sim (Full VAT)">
        <div class="product-item__box">
            <div class="product-item__left">
                <div class="product-item__thumb">
                    <img src="https://shopdunk.com/wp-content/uploads/2019/09/iP11Pro-3-150x150.jpg" alt="">
                </div>
                <div class="product-item__specifications">
                    <div class="product-item__rating">
                        <span class="star-rating">
                            <span style="width: 90%"></span>
                        </span>
                        <span>717 đánh giá</span>
                    </div>
                    <div class="product-item__color">
                        <h6>Màu sắc:</h6>
                        <p class="color-board">
                            <label style="background: red"></label>
                            <label style="background: gold"></label>
                            <label style="background: gray"></label>
                            <label style="background: black"></label>
                        </p>
                    </div>
                    <div class="product-item__capacity">
                        <h6>Dung lượng:</h6>
                        <p class="capacity-product">
                            <label>16GB</label>
                            <label>32GB</label>
                            <label>64GB</label>
                            <label>128GB</label>
                            <label>256GB</label>
                        </p>
                    </div>
                </div>
            </div>
            <div class="product-item__right">
                <h4 class="product-item__title">iPhone 11 Pro Max chính hãng Apple xuất xứ Mỹ (LL/A) 1 Sim (Full VAT)</h4>
                <p class="product-item__guarantee">Bảo hành 24 tháng</p>
                <div class="product-item__price">
                    <p><span class="d-none d-md-inline">Giá chưa KM: </span><span class="price price--original">20.100.000đ</span></p>
                    <p><span class="d-none d-md-inline">Giá đã KM: </span><span class="price price--promotion">16.750.000đ</span></p>
                    <p class="d-none d-md-block">Bạn tiết kiệm: <span class="price">3.350.000đ</span></p>
                </div>
                <div class="product-item__promotion">
                    <h6>Khuyến mại:</h6>
                    <div class="text-format">
                        <p>- BẢO HÀNH 24 THÁNG</p>
                        <p>- Trả góp 0% lãi suất thẻ tín dụng</p>
                        <p>- Giảm giá 20% (Cường lực, Ốp lưng)</p>
                    </div>
                </div>
                <div class="product-item__button-view">
                    <strong>Xem chi tiết</strong><br>
                </div>
            </div>
            <div class="product-item__description">
                <div class="text-format">
                    <p>iPhone 11 Pro Max chính hãng Apple xuất xứ Mỹ (LL/A) 1 Sim (Full VAT) giá tốt nhất: <strong>24.750.000₫</strong> (Màu Xám, 64GB)
                    </p>
                </div>
            </div>
        </div>
    </a>
</article>
