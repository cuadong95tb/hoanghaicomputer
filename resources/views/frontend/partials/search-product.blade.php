<form action="" id="search-product" class="search-product">
    <input type="text" class="search-product__text" placeholder="Bạn muốn mua gì?">
    <button type="submit" class="search-product__submit"><i class="fal fa-search"></i></button>
    <a href="#search-product" class="close-box"><i class="fal fa-times"></i></a>
    <div class="search-product__autocomplete">
        @for($i=0; $i<20; $i++)
            @include('frontend.partials.item-product-shortcut')
        @endfor
    </div>
</form>