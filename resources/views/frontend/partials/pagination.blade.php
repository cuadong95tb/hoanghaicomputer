<ul class="pagination">
    <li class="page-item"><a class="page-link" href="#" rel="prev"><i class="fal fa-angle-double-left"></i></a></li>
    <li class="page-item"><a class="page-link" href="#">1</a></li>
    <li class="page-item"><span class="page-link">...</span></li>
    <li class="page-item"><a class="page-link" href="#">22</a></li>
    <li class="page-item"><a class="page-link" href="#">23</a></li>
    <li class="page-item active"><span class="page-link">24</span></li>
    <li class="page-item"><a class="page-link" href="#">25</a></li>
    <li class="page-item"><a class="page-link" href="#">26</a></li>
    <li class="page-item"><span class="page-link">...</span></li>
    <li class="page-item"><a class="page-link" href="#">29</a></li>
    <li class="page-item"><a class="page-link" href="#" rel="next"><i class="fal fa-angle-double-right"></i></a></li>
</ul>