<article class="customer-feedback">
    <div class="customer-feedback__avatar">
        <img src="https://shopdunk.com/wp-content/uploads/2018/11/200x200.jpg" alt="">
    </div>
    <div class="customer-feedback__content">
        <h3 class="customer-feedback__title">Chị Nguyễn Phương Lan mua iPhone 7 Plus, tháng 1/2017</h3>
        <div class="customer-feedback__detail">
            "Mình chọn mua iPhone 6s ở Shopdunk vì giá hợp lý, bảo hành đổi trả trong 1 năm liền. Mình sẽ giới thiệu bạn bè mua điện thoại ở đây. Chúc Shop làm ăn ngày càng phát triển."
        </div>
    </div>
</article>