<article class="product-item-shortcut">
    <div class="product-item-shortcut__thumb">
        <a href="/frontend/products/detail">
            <img src="https://storage.googleapis.com/shopdunk-images/tintucshopdunknew/2019/01/iphone7plus-bac1-1-e1547509624564.png" alt="">
        </a>
    </div>
    <div class="product-item-shortcut__content">
        <h3 class="product-item-shortcut__title">
            <a href="/frontend/products/detail">iPhone Xs Max (2 Sim) 64GB</a>
        </h3>
        <div class="product-item-shortcut__price">{{-- Note: if hasn't sale then remove class 'has-sale' --}}
            <div class="price has-sale">
                <span class="price__original">28.090.000đ</span>
                <span class="price__sale">25.090.000đ</span>
            </div>
        </div>
    </div>
</article>
