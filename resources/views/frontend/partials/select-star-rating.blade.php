<div class="select-star-rating">
    <div class="select-star-rating__input">
        <input type="radio" id="5-stars" name="rating" value="5" />
        <label for="5-stars" title="Tuyệt vời">&#9733;</label>
        <input type="radio" id="4-stars" name="rating" value="4" />
        <label for="4-stars" title="Hài lòng">&#9733;</label>
        <input type="radio" id="3-stars" name="rating" value="3" />
        <label for="3-stars" title="Bình thường">&#9733;</label>
        <input type="radio" id="2-stars" name="rating" value="2" />
        <label for="2-stars" title="Tạm được">&#9733;</label>
        <input type="radio" id="1-star" name="rating" value="1" />
        <label for="1-star" title="Không thích">&#9733;</label>
    </div>
</div>