<header class="header">
    <div class="header-top">
        <div class="container d-flex justify-content-between align-items-center">
            <div class="header-top__left">
                <h1 class="header-top__slogan">Hoàng Hải Company</h1>
                <nav class="header-top__external-links">
                    <ul>
                        <li>
                            <a href="#" class="color-red">
                                <img src="{{asset('images/icon-red.png')}}" alt="">
                                Hoàng Hải
                            </a>
                        </li>
                        <li>
                            <a href="#" class="color-green">
                                <img src="{{asset('images/icon-green.png')}}" alt="">
                                Điện thoại xanh
                            </a>
                        </li>
                        <li>
                            <a href="#" class="color-ischool">
                                <img src="{{asset('images/icon-iSchool.png')}}" alt="">
                                iSchool
                            </a>
                        </li>
                        <li>
                            <a href="/frontend/blog" class="color-red">
                                <img src="{{asset('images/icon-red.png')}}" alt="">
                                Tin tức
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
            <div class="header-top__right">
                <nav class="header-top__menu">
                    <ul>
                        <li>
                            <a href="#">Khuyến mại</a>
                        </li>
                        <li>
                            <a href="#">Trả góp</a>
                            <ul>
                                <li><a href="#">Khu vực Hà Nội</a></li>
                                <li><a href="#">Khu vực Hồ Chí Minh</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="#">Tuyển dụng</a>
                        </li>
                    </ul>
                </nav>
                <nav class="header-top__hotline">
                    <ul>
                        <li>
                            <a href="tel:0981166641">0981.166.641</a>
                        </li>
                        <li>
                            <a href="tel:0981166641">0981.166.641</a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
    <div class="header-middle">
        <div class="container d-flex justify-content-between align-items-center position-relative">
            <div class="header-middle__logo">
                <a href="/frontend"><img src="{{asset('images/logo.png')}}" alt="Logo Hoàng Hải Computer"></a>
            </div>
            <div class="header-middle__search">
                <a href="#search-product" class="open-box d-sm-none"><i class="fal fa-search"></i></a>
                @include('frontend.partials.search-product')
            </div>
            <div class="header-middle__service">
                <ul>
                    <li class="service">
                        <div class="service__box">
                            <img class="service__icon" src="{{asset('images/service-guarantee.jpg')}}" alt="">
                            <div class="service__title">
                                <h4>Bảo hành</h4>
                                <span>1 đổi 1 lên tới 24 tháng</span>
                            </div>
                        </div>
                        <ul class="service__detail">
                            <li>
                                <strong>Hàng chính hãng (Full VAT)</strong><br>
                                <span>Bảo hành 1 đổi 1 lên tới 24 tháng</span>
                            </li>
                            <li>
                                <strong>Hàng Like New</strong><br>
                                <span>Bảo hành 1 đổi 1 lên tới 24 tháng</span>
                            </li>
                        </ul>
                    </li>
                    <li class="service">
                        <div class="service__box">
                            <img class="service__icon" src="{{asset('images/service-pioneers.jpg')}}" alt="">
                            <div class="service__title">
                                <h4>Tiên phong</h4>
                                <span>Đưa iPhone 11 đầu tiên về Việt Nam</span>
                            </div>
                        </div>
                        <ul class="service__detail">
                            <li><strong>Đầu tiên đưa iPhone X về Việt Nam</strong></li>
                            <li><strong>Đầu tiên đưa iPhone X về Việt Nam</strong></li>
                            <li><strong>Đầu tiên đưa iPhone X về Việt Nam</strong></li>
                            <li><strong>Đầu tiên đưa iPhone X về Việt Nam</strong></li>
                            <li><strong>Đầu tiên đưa iPhone X về Việt Nam</strong></li>
                            <li><strong>Đầu tiên đưa iPhone X về Việt Nam</strong></li>
                        </ul>
                    </li>
                    <li class="service">
                        <div class="service__box">
                            <img class="service__icon" src="{{asset('images/service-commit.jpg')}}" alt="">
                            <div class="service__title">
                                <h4>Cam kết</h4>
                                <span>Tặng 10 triệu đồng nếu phát hiện hàng dựng</span>
                            </div>
                        </div>
                        <ul class="service__detail">
                            <li><strong>Hàng chính hãng Apple</strong></li>
                            <li><strong>Hàng like new chưa qua sửa chữa</strong></li>
                            <li><strong>Hàng nhập khẩu, nguyên zin</strong></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <a href="/frontend/order" class="header-middle__cart" title="Giỏ hàng của bạn">
                <i class="fal fa-shopping-cart"></i>
                <span class="cart-count">0</span>
            </a>
            <a href="#menu-mobile" class="header-middle__button-menu open-box">
                <i class="fal fa-bars"></i>
            </a>
        </div>
    </div>
    <div id="menu-mobile" class="header-bottom">
        <div class="container">
            <a href="#menu-mobile" class="header-bottom__button-close close-box"></a>
            <nav class="nav-categories">
                <ul>
                    <li class="active"><a href="/frontend"><i class="fad fa-home-lg-alt"></i></a></li>
                    <li class="has-child">
                        <a href="/frontend/products/category">iPhone</a>
                        <ul>
                            <li class="has-child"><a href="">iPad <span class="status">New</span></a>
                                <ul>
                                    <li class="has-child"><a href="">iPad</a>
                                        <ul>
                                            <li><a href="">iPad</a></li>
                                            <li><a href="">Apple Watch</a></li>
                                            <li><a href="">MacBook</a></li>
                                            <li><a href="">Microsoft Surface</a></li>
                                            <li><a href="">Phụ kiện</a></li>
                                            <li><a href="">Điện thoại</a></li>
                                            <li><a href="">Apple Store</a></li>
                                            <li><a href="">Âm thanh</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="">Apple Watch</a></li>
                                    <li><a href="">MacBook</a></li>
                                    <li><a href="">Microsoft Surface</a></li>
                                    <li><a href="">Phụ kiện</a></li>
                                    <li><a href="">Điện thoại</a></li>
                                    <li><a href="">Apple Store</a></li>
                                    <li><a href="">Âm thanh</a></li>
                                </ul>
                            </li>
                            <li><a href="">Apple Watch</a></li>
                            <li><a href="">MacBook</a></li>
                            <li><a href="">Microsoft Surface</a></li>
                            <li><a href="">Phụ kiện</a></li>
                            <li><a href="">Điện thoại</a></li>
                            <li><a href="">Apple Store</a></li>
                            <li><a href="">Âm thanh</a></li>
                        </ul>
                    </li>
                    <li><a href="/frontend/products/category">iPad</a></li>
                    <li><a href="/frontend/products/category">Apple Watch</a></li>
                    <li><a href="/frontend/products/category">MacBook</a></li>
                    <li><a href="/frontend/products/category">Microsoft Surface</a></li>
                    <li><a href="/frontend/products/category">Phụ kiện</a></li>
                    <li><a href="/frontend/products/category">Điện thoại</a></li>
                    <li><a href="/frontend/products/category">Apple Store</a></li>
                    <li><a href="/frontend/products/category">Âm thanh</a></li>
                    <li><a href="/frontend/products/category">Ánh sáng</a></li>
                </ul>
            </nav>
        </div>
    </div>
</header>
