<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-md-3 col-xl-2">
                <div class="footer__col">
                    <h4 class="footer__title">Hệ thống Hoàng Hải</h4>
                    <ul class="footer__links">
                        <li><a href="#"><img src="{{asset('images/icon-red.png')}}" alt=""> Hoàng Hải</a></li>
                        <li><a href="#"><img src="{{asset('images/icon-green.png')}}" alt=""> Điện thoại xanh</a></li>
                        <li><a href="#"><img src="{{asset('images/icon-iSchool.png')}}" alt=""> iSchool</a></li>
                        <li><a href="/blog"><img src="{{asset('images/icon-red.png')}}" alt=""> Tin tức</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-6 col-md-3 col-xl-2">
                <div class="footer__col">
                    <h4 class="footer__title">Thông tin</h4>
                    <ul class="footer__links">
                        <li><a href="#">Giới thiệu</a></li>
                        <li><a href="#">Khuyến mại</a></li>
                        <li><a href="#">Tuyển dụng</a></li>
                        <li><a href="#">Showroom</a></li>
                        <li><a href="#">Hỏi & đáp</a></li>
                        <li><a href="#">Góp ý, khiếu nại</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-6 col-md-3 col-xl-2">
                <div class="footer__col">
                    <h4 class="footer__title">Chính sách</h4>
                    <ul class="footer__links">
                        <li><a href="#">Chính sách bảo mật</a></li>
                        <li><a href="#">Chính sách hoạt động</a></li>
                        <li><a href="#">Chính sách trả góp</a></li>
                        <li><a href="#">Chính sách bảo hành</a></li>
                        <li><a href="#">Hướng dẫn SHIP COD</a></li>
                        <li><a href="#">Hướng dẫn mua hàng</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-6 col-md-3 col-xl-2">
                <div class="footer__col">
                    <h4 class="footer__title">Liên hệ</h4>
                    <ul class="footer__links">
                        <li>Hotline: <a href="tel:19006626">1900.6626</a></li>
                        <li>Email: <a href="mailto:info@gmail.com">info@gmail.com</a></li>
                        <li><a href="#"><img class="bocongthuong" src="{{asset('images/bocongthuong.png')}}" alt=""></a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-6 col-lg-5 col-xl-4">
                <iframe
                        src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Finplacevietnam%2F&tabs=timeline&width=340&height=500&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId=317650975340807"
                        width="100%"
                        height="250"
                        style="border:none;overflow:hidden"
                        scrolling="no"
                        frameborder="0"
                        allowTransparency="true"
                        allow="encrypted-media"
                ></iframe>
            </div>
            <div class="col-md-6 col-lg-7 col-xl-12">
                <div class="row">
                    <div class="col-xl-7 py-0">
                        <div class="footer__copyright text-format">
                            © 2016 Công ty CP HESMAN Việt Nam <br>
                            Giấy chứng nhận ĐKDN số 0107465657 do Sở kế hoạch và đầu tư thành phố Hà Nội cấp ngày 08 tháng 06 năm 2016 <br>
                            Địa chỉ: Nhà N3-7 Dự án Xuân La, ngách 46, ngõ 191 Lạc Long >Quân, phường Xuân La, quận Tây Hồ, thành phố Hà Nội,
                            Việt Nam <br>
                            Điện thoại: <a href="tel:0981166642">0981.166.642</a> <br>
                            Email: <a href="mailto:manhhoa14286@gmail.com">manhhoa14286@gmail.com</a> <br>
                            Đại diện pháp luật: PHẠM MẠNH HÒA
                        </div>
                    </div>
                    <div class="col-xl-5 py-0">
                        <div class="footer__pay text-xl-right">
                            <img src="{{asset('images/pay-master-card.jpg')}}" alt="">
                            <img src="{{asset('images/pay-visa.jpg')}}" alt="">
                            <img src="{{asset('images/pay-money.jpg')}}" alt="">
                            <img src="{{asset('images/pay-transfer.jpg')}}" alt="">
                            <img src="{{asset('images/pay-atm.jpg')}}" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
