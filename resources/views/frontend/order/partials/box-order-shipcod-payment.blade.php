<div class="order-payment">
    <h3 class="order-payment__title">Thanh toán khi nhận hàng</h3>
    <div class="order-payment__content">
        <div class="text-format">
            <p>Quý khách thanh toán tiền mặt khi nhận hàng tại nhà.</p>
            <p>Với quý khách hàng đặt hàng trực tiếp tại Website <a href="www.jamobile.co.jp" target="_blank">www.jamobile.co.jp</a> sẽ được miễn phí dịch vụ Giao hàng tiết kiệm.</p>
            <p>Đặt hàng Trên Fanpage hay nhắn tin qua điện thoại phí dịch vụ là 1,500¥./.</p>
        </div>
    </div>
</div>
