<section class="cart-main__form-info">
    <h3 class="cart-main__title">Thông tin khách hàng</h3>
    <div class="form-info-customer">
        <div class="form-group">
            <div class="form-group__row">
                <label>Họ và tên: <span>*</span></label>
                <input
                        id="txt-name"
                        class="form-control"
                        type="text"
                        maxlength="255"
                        placeholder="Nhập họ tên của bạn"
                >
            </div>
            <div class="form-group__alert">
                <div data-error="txt-name" class="alert-error"></div>
            </div>
        </div>
        <div class="form-group">
            <div class="form-group__row">
                <label>Số điện thoại: <span>*</span></label>
                <input
                        name="phone_customer"
                        id="txt-phone"
                        class="form-control"
                        type="tel"
                        maxlength="255"
                        placeholder="Nhập số điện thoại của bạn"
                >
            </div>
            <div class="form-group__alert">
                <div data-error="txt-phone" class="alert-error"></div>
            </div>
        </div>
        <div class="form-group">
            <div class="form-group__row">
                <label>Email: <small>(Không bắt buộc)</small></label>
                <input
                        name="email_customer"
                        id="txt-email"
                        class="form-control"
                        type="email"
                        maxlength="255"
                        placeholder="Nhập Email của bạn"
                >
            </div>
            <div class="form-group__alert">
                <div data-error="txt-email" class="alert-error"></div>
                <small><em>Chi tiết đơn hàng sẽ được gửi vào email</em></small>
            </div>
        </div>
        <div class="form-group">
            <div class="form-group__row">
                <label>Địa chỉ: <span>*</span></label>
                <input
                    id="txt-address"
                    class="form-control"
                    type="text"
                    placeholder="Nhập địa chỉ của bạn"
                >
            </div>
            <div class="form-group__alert">
                <div data-error="txt-address" class="alert-error"></div>
            </div>
        </div>
        <div class="form-group">
            <div class="form-group__row">
                <label>Link Facebook: <span>*</span></label>
                <input
                    id="txt-link-facebook"
                    class="form-control"
                    type="url"
                    placeholder="Nhập link Facebook của bạn"
                >
            </div>
            <div class="form-group__alert">
                <div data-error="txt-link-facebook" class="alert-error"></div>
            </div>
        </div>
        <div class="form-group mb-0">
            <div class="form-group__alert">
                <button type="submit" class="cart-main__submit">Đặt hàng ngay<small>Chúng tôi sẽ liên hệ ngay đến quý khách!</small></button>
            </div>
        </div>
    </div>
</section>
