<section class="cart-empty">
    <div class="container">
        <div class="cart-empty__box">
            <div class="cart-empty__icon">
                <img src="{{asset('images/cart-empty.png')}}" alt="">
            </div>
            <div class="cart-empty__notification">
                Không có sản phẩm nào trong giỏ hàng của bạn!
            </div>
            <div class="cart-empty__comeback">
                <a href="/frontent">Tiếp tục mua sắm</a>
            </div>
        </div>
    </div>
</section>