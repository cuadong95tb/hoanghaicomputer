<div class="order-success">
    <section class="order-success__information">
        <h2 class="order-success__title">Thông tin đặt hàng</h2>
        <div class="p-3">
            <table class="table table-sm">
                <tr>
                    <td>Mã đơn hàng:</td>
                    <td>6265655</td>
                </tr>
                <tr>
                    <td>Hình thức thanh toán:</td>
                    <td>Thanh toán sau khi nhận hàng</td>
                </tr>
                <tr>
                    <td>Họ tên khách hàng:</td>
                    <td>Nguyễn Thanh An</td>
                </tr>
                <tr>
                    <td>Số điện thoại:</td>
                    <td>0978534589</td>
                </tr>
                <tr>
                    <td>Email:</td>
                    <td>nguyenthanhan@email.com</td>
                </tr>
                <tr>
                    <td>Địa chỉ:</td>
                    <td>Vũ Ngọc Phan, Láng Hạ, Đống Đa, Hà Nội</td>
                </tr>
                <tr>
                    <td>Facebook:</td>
                    <td><a href="https://fb.com/maile995" target="_blank">https://fb.com/maile995</a></td>
                </tr>
            </table>
        </div>
    </section>
    <section class="order-success__products">
        <h2 class="order-success__title">Sản phẩm bạn đã mua</h2>
        @for($i=0; $i<3; $i++)
            <div class="item-product-purchased">
                <div class="item-product-purchased__thumb">
                    <img src="https://cdn.fptshop.com.vn/Uploads/Thumbs/2020/1/7/637139916609041371_6s-pl-vang-1.png" alt="">
                </div>
                <div class="item-product-purchased__info">
                    <h6>Asus Vivobook X409FA-EK098T i3 8145U/4GB/1TB/14.0" FHD/Win10</h6>
                    <p>
                        Màu: <span>Vàng</span>
                    </p>
                    <p>
                        Số lượng: <span>1</span>
                    </p>
                </div>
                <div class="item-product-purchased__price">
                    <div class="price has-sale">{{-- Note: if hasn't sale then remove class 'has-sale' --}}
                        <p class="price__sale">1.000.000đ <small>(20.000¥)</small></p>
                        <p class="price__original">2.000.000đ <small>(40.000¥)</small></p>
                    </div>
                </div>
            </div>
        @endfor
    </section>
    <section class="order-success__calculator">
        <table>
            <tr>
                <td><strong>Tạm tính:</strong></td>
                <td><strong class="active">34.460.000đ <small>(40.000¥)</small></strong></td>
            </tr>
            <tr>
                <td><small>Khuyến mãi:</small></td>
                <td><small>34.460.000đ <small>(40.000¥)</small></small></td>
            </tr>
            <tr>
                <td><strong>Tổng tiền:</strong></td>
                <td><strong class="active">34.460.000đ <small>(40.000¥)</small></strong></td>
            </tr>
        </table>
    </section>
    <section class="order-success__comeback">
        <a href="/frontend" class="button-comback">Mua thêm sản phẩm khác</a>
    </section>
</div>
