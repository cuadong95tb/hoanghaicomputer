<div class="order-payment">
    <h3 class="order-payment__title">Thanh toán Chuyển khoản ngân hàng</h3>
    <div class="order-payment__content">
        <div class="text-format">
            <p>Quý khách vui lòng chuyển khoản Tổng số tiền đơn hàng vào 1 trong các tài khoản dưới đây. Với hình thức này quý khách được miễn phí vận chuyển toàn Nhật Bản.</p>
            <p>
                <em><strong>Lưu ý:</strong></em><br>
                <em>Với các khách hàng tại Việt Nam muốn đặt hàng hay quý khách hàng muốn thanh toán tiền Việt Nam đồng VNĐ vui lòng chuyển khoản vào tài khoản ngân hàng Vietcombank ở dưới.</em><br>
            </p>
            <p>
                <strong>Số tiền được quy đổi như sau:</strong><br>
                <span>Tổng giá trị đơn hàng X Tỉ giá giao dịch (Cột cuối – Giá bán tại ngân hàng)</span><br>
                <span>Ví dụ: 28,000¥ X 220 = 6,160,000VNĐ</span><br>
                <em>(Giá tiền quy đổi chưa bao gồm phí vận chuyển về Việt Nam)</em>
            </p>
            <h4>Thông tin chuyển khoản ngân hàng</h4>
            <h6>Ngân hàng TP Bank:</h6>
            <ul>
                <li>Chủ tài khoản: <strong>Hoàng Văn Hải</strong></li>
                <li>Số tài khoản: <strong>00364724802</strong></li>
                <li>Chi nhánh: <strong>TP Bank chi nhánh Phạm Hùng</strong></li>
            </ul>
            <h6>Ngân hàng Techcombank:</h6>
            <ul>
                <li>Chủ tài khoản: <strong>Hoàng Văn Hải</strong></li>
                <li>Số tài khoản: <strong>00364724802</strong></li>
                <li>Chi nhánh: <strong>Teckcombank Ngọc Khánh</strong></li>
            </ul>
            <h6>Ngân hàng Vietcombank:</h6>
            <ul>
                <li>Chủ tài khoản: <strong>Hoàng Văn Hải</strong></li>
                <li>Số tài khoản: <strong>00364724802</strong></li>
                <li>Chi nhánh: <strong>Vietcombank Ba Đình</strong></li>
            </ul>
        </div>
    </div>
</div>
