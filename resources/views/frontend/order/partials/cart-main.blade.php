<div class="cart-main">
    <div class="container">
        <form action="/frontend/order/send" class="form-cart" method="POST">
            <div class="row">
                <div class="col-md-10 col-lg-8 col-xl-6 offset-md-1 offset-lg-2 offset-xl-3">
                    <section class="cart-main__products">
                        <h3 class="cart-main__title">Giỏ hàng của bạn <small>(2 sản phẩm)</small></h3>
                        <div class="list-products">
                            @for($i=0; $i<3; $i++)
                            <div class="product-cart">
                                <div class="product-cart__thumb">
                                    <a href="/frontend/products/detail">
                                        <img src="https://cdn.fptshop.com.vn/Uploads/Thumbs/2020/1/7/637139916609041371_6s-pl-vang-1.png" alt="">
                                    </a>
                                </div>
                                <div class="product-cart__info">
                                    <h6 class="product-cart__title">
                                        <a href="/frontend/products/detail">Asus Vivobook X409FA-EK098T i3 8145U/4GB/1TB/14.0" FHD/Win10</a>
                                    </h6>
                                    <div class="product-cart__color">
                                        Màu:
                                        <p class="color-board choose-color">
                                            <label class="active" style="background: red"></label>
                                            <label style="background: gold"></label>
                                            <label style="background: gray"></label>
                                            <label style="background: black"></label>
                                            <label style="background: orange"></label>
                                        </p>
                                    </div>
                                </div>
                                <div class="product-cart__price">
                                    <div class="price has-sale">{{-- Note: if hasn't sale then remove class 'has-sale' --}}
                                        <p class="price__original">2.000.000đ <small>(40.000¥)</small></p>
                                        <p class="price__sale">1.000.000đ <small>(20.000¥)</small></p>
                                        <p class="price__note">Giảm 17%</p>
                                    </div>
                                </div>
                                <div class="product-cart__action">
                                    <div class="quantity">
                                        <input type="number" min="1" max="4" value="1">
                                        <span class="quantity__up">+</span>
                                        <span class="quantity__down">-</span>
                                    </div>
                                    <button class="product-cart__delete remove-from-cart" title="Xóa khỏi giỏ hàng">
                                        <i class="fal fa-times"></i>
                                    </button>
                                </div>
                            </div>
                            @endfor
                        </div>
                        <div class="cart-main__total">
                            <div class="row">
                                <div class="col-sm-6 mb-2 mb-sm-0">
                                    <div class="cart-main__discount">
                                        <a href="" class="use-discount">Sử dụng mã giảm giá</a>
                                        <div class="form-discount">
                                            <div class="d-flex">
                                                <input type="text" class="form-discount__text" placeholder="Nhập mã giảm giá">
                                                <button class="form-discount__button">Áp dụng</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="cart-main__calculator">
                                        <table>
                                            <tr>
                                                <td><strong>Tạm tính:</strong></td>
                                                <td><strong class="active">34.460.000đ <small>(40.000¥)</small></strong></td>
                                            </tr>
                                            <tr>
                                                <td><small>Khuyến mãi:</small></td>
                                                <td><small>34.460.000đ <small>(40.000¥)</small></small></td>
                                            </tr>
                                            <tr>
                                                <td><strong>Tổng tiền:</strong></td>
                                                <td><strong class="active">34.460.000đ <small>(40.000¥)</small></strong></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section class="cart-main__payments">
                        <h3 class="cart-main__title">Hình thức thanh toán</small></h3>
                        <div class="select-payment">
                            <label>
                                <input type="radio" name="payment" value="0">
                                <span>Chuyển khoản ngân hàng</span>
                            </label>
                            <label>
                                <input type="radio" name="payment" value="1">
                                <span>Trả tiền mặt khi nhận hàng (COD)</span>
                            </label>
                            <label>
                                <input type="radio" name="payment" value="2">
                                <span>Đến mua trực tiếp tại cửa hàng</span>
                            </label>
                            <div data-error="radio-payment" class="alert-error"></div>
                        </div>
                    </section>
                    @include('frontend.order.partials.form-customer-infomation')
                    <section class="cart-main__note">
                        <p>Bằng cách đặt hàng, bạn đồng ý với <a href="#">Điều khoản sử dụng</a> của Hoàng Hải Computer</p>
                        <a href="/frontend" class="cart-main__comeback">Mua thêm sản phẩm khác</a>
                    </section>
                </div>
            </div>
        </form>
    </div>
</div>
