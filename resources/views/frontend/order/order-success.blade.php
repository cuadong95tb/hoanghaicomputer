@extends('frontend.layouts.master')
@section('meta')
    <title>Đặt hàng thành công!</title>
@stop
@section('stylesheet')
    <link rel="stylesheet" href="{{asset('assets/order/order-success.css')}}">
@stop
@section('content')
    @php
    $payment = 0;
    @endphp
{{--    --Value $payment----}}
{{--    0: Chuyển khoản ngân hàng--}}
{{--    1: Ship COD--}}
{{--    2: Đến mua tại cửa hàng--}}

    <main class="order-success-page">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-lg-8 col-xl-6 offset-md-1 offset-lg-2 offset-xl-3">
                    <section class="order-success-page__head">
                        <i class="fal fa-check"></i>
                        <h1>Cảm ơn quý khách đã mua hàng tại Hoàng Hải Computer!</h1>
                        <p>
                            Tổng đài viên của Hoàng Hải Computer sẽ liên hệ đến quý khách trong vòng 5 phút <br>
                            Xin cảm ơn quý khách đã cho chúng tôi cơ hội được phục vụ!
                        </p>
                    </section>
                </div>
                @switch($payment)
                    @case(0)
                    <div class="col-md-10 col-lg-8 col-xl-6 offset-md-1 offset-lg-2 offset-xl-0 d-flex">
                        @include('frontend.order.partials.box-order-transfer-payment')
                    </div>
                    @break
                    @case(1)
                    <div class="col-md-10 col-lg-8 col-xl-6 offset-md-1 offset-lg-2 offset-xl-0 d-flex">
                        @include('frontend.order.partials.box-order-shipcod-payment')
                    </div>
                    @break
                    @default
                @endswitch
                <div class="col-md-10 offset-md-1 col-lg-8 offset-lg-2 col-xl-6 offset-xl-{{$payment < 2 ? '0':'3'}}">
                    @include('frontend.order.partials.box-order-success')
                </div>
            </div>
        </div>
    </main>
@stop
@section('script')
    <script src="{{asset('assets/order/order-success.js')}}"></script>
@stop
