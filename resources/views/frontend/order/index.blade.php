@extends('frontend.layouts.master')
@section('meta')
    <title>Giỏ hàng</title>
@stop
@section('stylesheet')
    <link rel="stylesheet" href="{{asset('assets/order/index.css')}}">
@stop
@section('content')
    <main class="cart-page">
{{--        @include('frontend.order.partials.cart-empty')--}}
        @include('frontend.order.partials.cart-main')
    </main>
@stop
@section('script')
    <script src="{{asset('assets/order/index.js')}}"></script>
@stop