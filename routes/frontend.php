<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('frontend')->group(function(){
    Route::get('/', function () {
        return view('frontend.home.index');
    });

    Route::prefix('/products')->group(function(){
        Route::get('/category', function () {
            return view('frontend.products.category');
        });
        Route::get('/detail', function () {
            return view('frontend.products.detail');
        });
    });

    Route::prefix('/blog')->group(function(){
        Route::get('/', function () {
            return view('frontend.blog.index');
        });
        Route::get('/category', function () {
            return view('frontend.blog.category');
        });
        Route::get('/detail', function () {
            return view('frontend.blog.detail');
        });
    });

    Route::prefix('/order')->group(function(){
        Route::get('/', function () {
            return view('frontend.order.index');
        });
        Route::get('/success', function () {
            return view('frontend.order.order-success');
        });
        Route::post('/send', function () {
            dd(123);
        });
    });
});

