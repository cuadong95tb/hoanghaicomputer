const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/home/index.js', 'public/assets/home')
    .sass('resources/sass/pages/home/index.scss', 'public/assets/home');


//products
mix.js('resources/js/products/category.js', 'public/assets/products')
    .sass('resources/sass/pages/products/category.scss', 'public/assets/products');

mix.js('resources/js/products/detail.js', 'public/assets/products')
    .sass('resources/sass/pages/products/detail.scss', 'public/assets/products');

//order
mix.js('resources/js/order/index.js', 'public/assets/order')
    .sass('resources/sass/pages/order/index.scss', 'public/assets/order');

mix.js('resources/js/order/order-success.js', 'public/assets/order')
    .sass('resources/sass/pages/order/order-success.scss', 'public/assets/order');


//blog
mix.js('resources/js/blog/index.js', 'public/assets/blog')
    .sass('resources/sass/pages/blog/index.scss', 'public/assets/blog');

mix.js('resources/js/blog/category.js', 'public/assets/blog')
    .sass('resources/sass/pages/blog/category.scss', 'public/assets/blog');

mix.js('resources/js/blog/detail.js', 'public/assets/blog')
    .sass('resources/sass/pages/blog/detail.scss', 'public/assets/blog');


